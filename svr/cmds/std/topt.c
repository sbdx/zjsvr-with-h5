#include <ansi.h>
#include <mudlib.h>

inherit F_CLEAN_UP;
int top_list(object,object);
int get_score(object);
int sort_exp_rank(object, object);
int sort_reborns_exp(object,object);
int sort_neili_exp(object,object);
int sort_jifen(object, object);

int sort_keys(int a, int b){return a-b;}


int filter_listenr(object ob,int ages,int agee){
	if ( userp(ob) 
			//&& !wizardp(ob) 
			&&  environment(ob) 
			//&& ob->query("reborns/count")<=0
			&& ob->query("combat_exp")>3000 
			&& ob->query("age")>=ages 
			&& ob->query("age")<=agee 
	){
		return 1;
	}else{
		return 0;
	}
}


string get_space_str(string s, int n){
		
		string str="";
		int k,i;
		
		k = n - sizeof(s);
		k /=2;
		for( i = 0; i < k; i++ ){
			str+="・・・・";
		}
		return str;
	
}

string get_content(string title,int ages,int agee){
	string msg;
	object *list,*ob;
	int i,k=6;
	
	
	msg="";
		
	
		ob = filter_array(users(),(:filter_listenr:),ages,agee);
	   list = sort_array(ob, (: top_list :));
	   if (sizeof(list)<=0) msg += "暂时空缺\n";
	   i=1;
	   
	   foreach(object u in list){
		   
		   
		   //msg+=chinese_number(i)+" "+u->query("name")+ get_space_str(u->query("name"),k) +"战力 "+get_score( u )+"\n";
		   //msg+=sprintf(HIW"%s "+HIC" %-22s "+HIY" %s"+HIR"战力 %d\n",chinese_number(i),u->query("name")+"("+capitalize(u->query("id"))+")",u->query("family"),get_score( u ));
		   msg += sprintf(HIW"  %-5s"+HIC" %-22s"+HIY"%-10s"+HIR" %5d  "NOR"\n",chinese_number(i),u->query("name")+"("+capitalize(u->query("id"))+")", u->query("family")?  u->query("family/family_name"):"普通百姓",get_score(u));
		 					//排名	  名字 id		门派
		   //msg+=sprintf("%s %-─8s 战力 %d\n",chinese_number(i),u->query("name"),get_score( u ));
		   // msg += sprintf("%s %s\n",chinese_number(i),u->query("name"));
		 //  msg += sprintf("\t\t\t\t\战力 %d\n",get_score( u ));
		 //  msg += "────────────────────";
		   i++;
		   if (i>10) break;
	   }
	   

	 msg += "\n";
	 msg += "  " + NATURE_D->game_time() + "记。\n";
	 if(filter_listenr(this_player(),ages,agee)){
	 msg += HIM"您的评价是："+get_score(this_player())+"\n"NOR"\n";
	 }else{
	 msg += HIM"您的年龄不符合此榜单要求\n"NOR;
	 }
	   return msg;
}




int main(object me, string arg)
{
	
	object *list,*ob,*obs,obj;
	int i,a,ti;
	string msg,ts;
	
	
	string pops;
	string pop_title_text;
	

	if(!arg){
		write("参数不正确!\n");
		return 1;
	}
	
	
	if ( me->is_phone() ){
		pops= ZJOBLONG+"%s\n";
	}else{
		pops= "%s\n\n";
	}

	
	if (sscanf(arg,"%s %d",ts,ti)==2){
		if (ts=="combat"){//战力排行
			//少年组玩家14-18
					//青年组玩家 19-25
					//中年组玩家 26-40
					//老年组玩家 41-60
			
			if (ti==1){//少年组
				msg = "少年组";
				pops += get_content("少年组",14,18) ;
			}else if (ti==2){
				msg = "青年组";
				pops += get_content("青年组",19,30);
			}else if (ti==3){
				msg = "中年组";
				pops += get_content("中年组",31,50) ;
					
			}else if (ti==4){
				msg = "老年组";
				pops += get_content("老年组",51,90);
					
			}
			pop_title_text =  HIG+MUD_NAME+" "+msg+" 十大高手"NOR" \n";
		
			
		}
		
		
		
	}else{
		
		if( sscanf(arg,"%s",ts) ==1 ){
			

			
			
			if (ts=="combat"){//战力排行

					pop_title_text = "战力排行榜";
					pops= ZJOBLONG"战力排行榜\n";
					pops+=ZJOBACTS2+ZJMENUF(4,4,9,32);
					pops += "少年组:topt combat 1"+ZJSEP;
					pops += "青年组:topt combat 2"+ZJSEP;
					pops += "中年组:topt combat 3"+ZJSEP;
					pops += "老年组:topt combat 4"+ZJSEP;
					
					pops = sprintf(pops ,pop_title_text);
					write(pops+"\n");
					return 1;
					
					
					
			}
			if (ts=="exp"){//战力排行
								
					pop_title_text =HIG+MUD_NAME+" "+msg+" 武学排行榜"NOR" \n";
					obs = filter_array(users(),(:userp($1)/*&& !wizardp($1)/*/:) );
					list = sort_array(obs, (: sort_exp_rank :));
					
					list = list[0..9];
					 i=1;
					 foreach(obj in list){
					//	 pops += sprintf(HIW"  %-5s"+HIC" %-22s"+HIY"%5d  "NOR"\n",chinese_number(i),obj->query("name")+"("+capitalize(obj->query("id"))+")",RANK_D->query_exp_rank(obj));
					pops += sprintf(HIW"%s "+HIC"%s "+HIY"%d "+NOR"%s\n",chinese_number(i),obj->query("name")+"("+capitalize(obj->query("id"))+")", obj->query("combat_exp"),RANK_D->query_exp_rank(obj));
						 i++;
					   
					 }
								
							
								
			}
			
			
			
			if (ts=="neili"){//内力排行
								
								
				pop_title_text =HIG+MUD_NAME+" "+msg+" 内力排行榜"NOR" \n";
				obs = filter_array(users(),(:userp($1)  /*&& !wizardp($1)*/ :) );
				list = sort_array(obs, (: sort_neili_exp :));
				
				list = list[0..9];
				i=1;
				foreach(obj in list){
				pops += sprintf(HIW"%s "+HIC"%s "+HIG" %d "+NOR" %s\n",chinese_number(i),obj->query("name")+"("+capitalize(obj->query("id"))+")", obj->query("max_neili"),"/cmds/std/look.c"->gettof_neili(obj));
				   
					   i++;
				}
			}
			
			if (ts=="qi"){//气血排行
								
								
				pop_title_text =HIG+MUD_NAME+" "+msg+" 气血排行榜"NOR" \n";
				obs = filter_array(users(),(:userp($1)  /*&& !wizardp($1)*/ :) );
				list = sort_array(obs, (: sort_exp_rank :));
				
				list = list[0..9];
				i=1;
				foreach(obj in list){
					 pops += sprintf(HIW"%s "+HIC"%s "+HIR" %d "+NOR" %s\n",chinese_number(i),obj->query("name")+"("+capitalize(obj->query("id"))+")", obj->query("max_neili"),"/cmds/std/look.c"->gettof_qi(obj));
				   
					   i++;
				}
			}
			
			/*if(ts=="biwu"){
					mapping data=([]);
					int *key_list=({});
					
					pops = ZJOBLONG+"比武排行榜"+"\n";
					pops += ZJOBACTS2+ZJMENUF(1,1,9,32);
					data = "/adm/daemons/competed"->get_top_list("biwu",1,10);
					key_list = keys(data);
					key_list = sort_array(key_list, (: sort_keys :));
					foreach( int k in key_list ){
						pops += sprintf("第%s名 %s:pk of %s %d"ZJSEP,chinese_number(k),data[k][1],"biwu",k);
					}
					tell_object(me,pops+"\n");
					
					return 1;
					
					
			}/**/
			
			
			
		
			

		
			
		}
	}
	
	pops = sprintf(pops,pop_title_text);
	
	if (me->is_phone()){
		pops = replace_string(pops,"\n",ZJBR);
	}
	
	write(pops+"\n");
	
	

	return 1;
	

}



//排序最多转世
int sort_reborns_exp(object ob1, object ob2)
{
      	int a,b;
      	a = ob1->query("reborns/count");
		b = ob2->query("reborns/count");
		return b - a;
}


//积分排行榜
int sort_jifen(object ob1, object ob2)
{
      	int a,b;
      	a = ob1->query("jifen");
		b = ob2->query("jifen");
		return b - a;
}





//排序最大内力
int sort_neili_exp(object ob1, object ob2)
{
      	int a,b;
      	a = ob1->query("max_neili");
		b = ob2->query("max_neili");
		return b - a;
}

//排序最大气血
int sort_qi_exp(object ob1, object ob2)
{
      	int a,b;
      	a = ob1->query("max_qi");
		b = ob2->query("max_qi");
		return b - a;
}



//排序经验
int sort_exp_rank(object ob1, object ob2)
{
      	int a,b;
      	a = ob1->query("combat_exp");
		b = ob2->query("combat_exp");
		return b - a;
}



int top_list(object ob1, object ob2)
{
      int score1,score2;

	score1 = get_score(ob1);
	score2 = get_score(ob2);

      return score2 - score1;
}

int get_score(object ob)
{
	int tlvl,i,score;
	string *ski;
	mapping skills;

      reset_eval_cost();
	skills = ob->query_skills();
	if (!sizeof(skills)) return 1; 
	ski  = keys(skills);
	for(i = 0; i<sizeof(ski); i++) {
			tlvl += skills[ski[i]];
			}  // count total skill levels
	score = tlvl/10;
	score += ob->query("max_neili")/10;
	score += ob->query_str() + ob->query_int() + ob->query_dex() + ob->query_con();
	score += (int)ob->query("combat_exp")/2500;

	return score;
}

int help(object me)
{
write(@HELP
指令格式 : top 
 
这个指令可以让你知道在线十大高手是哪些，别去惹他们。 
 
HELP
    );
    return 1;
}
