// sp.c

#include <ansi.h>
#include <mudlib.h>

inherit F_CLEAN_UP;

void create() { seteuid(getuid()); }

int main(object me, string arg)
{
	string id,txt,*line;
	int i,k,sum=0,sum2;
	mapping m;

	if(!arg || sscanf(arg,"%s %d",id,k)!=2)
		return notify_fail("格式错误！\n");

	if(!(m = VIP_D->query_qian(id)))
		return notify_fail("没有这个推广员！\n");

	line = keys(m);
	for(i=0;i<sizeof(line);i++)
	{
		if(line[i]=="jf_used") continue;
		sum += (m[line[i]]-1);
	}
	if(sum<0) sum = 0;
	sum2 = VIP_D->query_qian(id+"/jf_used");

	if(k<1)
		return notify_fail(id+"的积分："+(sum-sum2)+"/"+sum+"。\n");

	if(k>(sum-sum2))
		return notify_fail("你没有那么多可用积分了！\n");

	sum2 += k;
	VIP_D->save();
	VIP_D->set_qian(id+"/jf_used",sum2);
	log_ufile(me,"tg2r","为 "+id+" 提现 "+k+" 积分。\n");
	write("你为 "+id+" 提现 "+k+" 积分。\n");

	return 1;
}

int help (object me)
{
	write(@HELP
指令格式: tgm 
设定推广员
HELP );
	return 1;
}
