//advertise.c
//推广系统:推广与被推广人双向绑定
//add by white,2020-04-28

#define ADVERTISE "/data/advertise.o" 
//inherit F_DBASE;
//inherit F_SAVE;
private int check_legal_id(string arg);

int NEEDCOMBATEXP = 1500000;

int main(object me, string arg)
{
	//推广人
	object ob,login_ob,login_me;
	//被推广人id
	string advId;
	string tempstr;
	string *lines,*info;
	int i,j;
	mapping mapadv;

	//没有参数，返回false
	if (!arg || arg == ""){
		return 0;
	}
		
	//id是否合法
	if (!check_legal_id(arg)){
		return 0;
	}

	advId = lower_case(arg);

	//自己不能做自己的推广员
	if (me->query("id") == advId){
		tell_object(me,"您不能做自己的引荐人。\n");
		return 1;
	}
	
	//巫师是否能做推广员？(应该可以,屏蔽掉)
	/*if (wizardp(me)){
		write("巫师不能做推广员。\n");
		return 0;
	}*/
		
	
	//当前玩家经验是否已经超出限制
	if (me->query("combat_exp") > NEEDCOMBATEXP)
	{
		tell_object(me,"您的经验已经不需要引荐人了吧！ \n");
		return 1;
	}

	//当前玩家是否已经有推广员
	//write((string)me->query("advertise")+"\n");
	if (me->query("advertise")){
		tell_object(me,"您已经有引荐人了吧！ \n");
		return 1;
	}
	//当前玩家是否绑定手机
	if (!me->query("telephone")){
		tell_object(me,"您需要先绑定手机才能绑定引荐人。请在巫师会客室绑定手机。\n");
		return 1;
	}
	//是否有对应推广员玩家
	ob = new(LOGIN_OB);
	ob->set("id", advId);
	if (!ob->restore())
	{
		destruct(ob);
		write("没有对应引荐人。\n");
		return 1;
	}
	
	//推广员条件是否达到
	if (!ultrap(ob)){
		tell_object(me, "您的引荐人必须是大宗师。\n");
		return 1;
	}

	//当前玩家对应手机是否已经有了推广员
	tempstr = read_file(ADVERTISE);
	//如果文件存在且不为空，将原有数据读出，并查询
	if (tempstr && tempstr != ""){
		lines = explode(tempstr, "\n");
		for (i = 0; i < sizeof(lines); i++){
			info = explode(lines[i], "║");
			//如果字串格式正确，判断当前手机是否存在
			if (sizeof(info) == 4){
				if (me->query("telephone") == info[2]){
					tell_object(me, "当前用户手机号码已经被推广，不需要重复被推广啦。\n\n");
					//销毁new出来的ob
					if (ob)
						destruct(ob);
					return 1;
				}					
			}
		}
	}

	//当前玩家login档案
	login_me = new(LOGIN_OB);
	login_me->set("id", me->query("id"));
	if (!login_me->restore()){
		tell_object(me, "用户档案出错，请联系巫师\n");
		destruct(login_me);
		if (ob)
			destruct(ob);
	}
	//当前玩家是否已经有了推广人
	//能进入这个if，说明原始文件遭到人为破坏
	if (login_me->query("byAdvertise")){
		tell_object(me, "您已经绑定了推广人，无需再次推广。\n");
		destruct(login_me);
		return 1;
	}
	else{
		//记录自身o文件中
		login_me->set("byAdvertise", (string)ob->query("id"));
		login_me->save();
		destruct(login_me);
	}
	
	
	//记录到推广员玩家o文件中
	ob->set("advertise/" + (string)me->query("id"), (string)me->query("telephone"));
	ob->save();
	//销毁new出来的ob
	if (ob)
		destruct(ob);

	//记录到全局系统文件中，格式（║分隔）：推广人id，被推广人id，手机号，时间戳
	tempstr = sprintf("%s║%s║%s║%d\n", advId, me->query("id"), me->query("telephone"),time());
	if (!write_file(ADVERTISE, tempstr, 0)){
		tell_object(me,"系统错误，绑定失败，请重试。\n\n");
		return 1;
	}
	tell_object(me, "推广人绑定成功\n");
	return 1;
}

int check_legal_id(string id)
{
	int i;

	id = lower_case(id);

	i = strlen(id);

	if ((i<4 || i>20) && (string)SECURITY_D->get_status(id) == "(player)") {
		write(ZJTMPSAY"对不起，你的ID长度不符合要求(4-20)。\n");
		return 0;
	}

	if (id[0]<'a' || id[0]>'z') {
		write(ZJTMPSAY"对不起，你的ID必须用英文字母开头。\n");
		return 0;
	}
	while (i--)
	{
		if (!((id[i] >= 'a' && id[i] <= 'z') || id[i] == '_' || (id[i] >= '0' && id[i] <= '9'))) {
			write(ZJTMPSAY"对不起，你的ID只能用英文字母或包含数字及‘_’的组合。\n");
			return 0;
		}
	}

	return 1;
}