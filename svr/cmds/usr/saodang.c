// arg.c
#include <ansi.h>
inherit F_CLEAN_UP;
int main(object me, string arg)
{
	object where = environment(me);
	int cishu;
	int shengyu;//剩余师门次数
    int GMT = time() + localtime(0)[8];
	string d, times;
	int suanfa;
	//师门奖励初始值
    int exp=500;
    int pot=700;
    int mar=50;
	int zhuanshi=me->query("zhuanshi");
   //转世后加成 五分之一*一次转世
    exp+=exp/5*zhuanshi;
    pot+=pot/5*zhuanshi;
    mar+=mar/5*zhuanshi;	
	times = ctime(GMT);
	//Sun(星期日) Mon(星期一)  Tue(星期二)  Wed(星期三)  Thu(星期四)  Fri(星期五)  Sat(星期六)
	d = times[0..2];//星期
		
	if (arg=="xianshi")
	{
		string msg;
		//msg  = ZJOBLONG;

		msg = ZJOBLONG + HIR "◎"HIY "师门扫荡" HIR "◎"NOR + ZJBR;
		msg += HIG"――――――――――――――――――――――"NOR + ZJBR;
		msg += "10黄金扫荡100师门 收益50%/40元宝票扫荡100师门 收益70%/20元宝扫荡100师门 收益80%\n";
		msg += ZJOBACTS2 + ZJMENUF(2, 2, 9, 30);
		msg += "黄金扫荡:saodang huangjin"ZJSEP;
		msg += "元宝票扫荡（40）:saodang piao"ZJSEP;
		msg += "元宝扫荡（20）:saodang yuanbao\n"ZJSEP;
		//msg += "刷新扫荡:saodang shuaxin\n";//此指令为巫师专用
	  //msg += "选择扫荡:saodang\n";//此指令移除
		message("vision", msg, me);
	}

	//if (arg=="shuaxin")//刷新
	//{
 // 
	//	//if (!(d == "Mon")) 
	//		//return notify_fail("你的扫荡只能在星期一刷新。\n");
 //       me->delete("shimen/saodang");
	//	me->set("saodang",0);
	//
	//	return  notify_fail("刷新成功。\n");
	//}	
	
	if (arg=="huangjin")//黄金扫荡
	{
		if (!living(me))  return 0;//离线限制
   		if (me->is_busy())
			return notify_fail("你现在正忙着呢。\n");
		if (environment(me)->query("no_learn") || environment(me)->query("no_fight") && (me->query("doing") != "scheme" || this_player(1)))
			return notify_fail("这里太纷杂，你没法作扫荡。\n"); 
		if (me->is_fighting())
			return notify_fail("临阵磨枪？来不及啦。\n");
		
		if (me->query("shimen/today")>=2000)//师门次数总数
		{
			return notify_fail("你的扫荡此时已经用完啦。\n");
		}
		if (me->query("shimen/today")<100)//师门次数总数
		{
			return notify_fail("你的扫荡必须手动100次才可以扫荡。\n");
		}
		if(me->query("balance")<100000)//黄金限制
		{
        return notify_fail("你钱庄的钱不足10黄金\n");
        }
	    if (! me->query("family"))
		{
		return notify_fail("你并不属于任何门派，你必须先加入一个\n");
		}
		if (  time()-(int)me->query("shimen/saodang")<3)
		{
			return notify_fail("时间没有过3秒暂时无法扫荡\n");
		}
		me->set("shimen/saodang",time());	
		me->start_busy(1);//一秒忙乱防止多点
		
        shengyu=(2000-me->query("shimen/today"));//剩余次数为
		
		
	me->add("shimen/today",100);
	me->add("balance",-100000);
	me->add("combat_exp",exp*50);
	me->add("potential",pot*50);
	me->add("experience",mar*50);
	me->add("gongxian",50);
	
	tell_object(me,HIC"你花费10黄金扫荡了100师门获得了\n");
	tell_object(me,HIG+exp*50+"经验\n");
	tell_object(me,HIG+pot*50+"潜能\n");
	tell_object(me,HIG+mar*50+"实战体会\n");
	tell_object(me,HIG+"50师门贡献\n");
	tell_object(me,ZJURL("cmds:saodang huangjin")ZJSIZE(20)"继续扫荡"NOR+"\n");		
		return  1;			
	}
	//元宝票扫荡
	if (arg=="piao")
	{
   		if (me->is_busy())
			return notify_fail("你现在正忙着呢。\n");
	    if (!living(me))  return 0;//离线限制
		if (environment(me)->query("no_learn") || environment(me)->query("no_fight") && (me->query("doing") != "scheme" || this_player(1)))
			return notify_fail("这里太纷杂，你没法作扫荡。\n"); 
		if (me->is_fighting())
			return notify_fail("临阵磨枪？来不及啦。\n");		
		if (me->query("shimen/today")>=2000)//师门次数总数
		{
			return notify_fail("你的扫荡此时已经用完啦。\n");
		}
		if (me->query("shimen/today")<100)//师门次数总数
		{
			return notify_fail("你的扫荡必须手动100次才可以扫荡。\n");
		}
		if(me->query("yuanbao_2")<100)//元宝票限制
			{
          return notify_fail("你的元宝票不足100\n");
         }
	    if (! me->query("family"))
		{
		return notify_fail("你并不属于任何门派，你必须先加入一个\n");
		}
		if (  time()-(int)me->query("shimen/saodang")<3)
		{
			return notify_fail("时间没有过3秒暂时无法扫荡\n");
		}
		me->set("shimen/saodang",time());	
		me->start_busy(1);//一秒忙乱防止多点
		
        shengyu=(2000-me->query("shimen/today"));//剩余次数为
		
		
	me->add("shimen/today",100);
	me->add("yuanbao_2",-40);
	me->add("combat_exp",exp*70);
	me->add("potential",pot*70);
	me->add("experience",mar*70);
	me->add("gongxian",70);
	
	tell_object(me,HIC"你花费40元宝票扫荡了100师门获得了\n");
	tell_object(me,HIG+exp*70+"经验\n");
	tell_object(me,HIG+pot*70+"潜能\n");
	tell_object(me,HIG+mar*70+"实战体会\n");
	tell_object(me,HIG+"70师门贡献\n");
	tell_object(me,ZJURL("cmds:saodang piao")ZJSIZE(20)"继续扫荡"NOR+"\n");		
		return  1;			
	}

	//元宝扫荡
	if (arg=="yuanbao")
	{
   		if (me->is_busy())
			return notify_fail("你现在正忙着呢。\n");
		 if (!living(me))  return 0;//离线限制
		if (environment(me)->query("no_learn") || environment(me)->query("no_fight") && (me->query("doing") != "scheme" || this_player(1)))
			return notify_fail("这里太纷杂，你没法作扫荡。\n"); 
		if (me->is_fighting())
			return notify_fail("临阵磨枪？来不及啦。\n");		
		if (me->query("shimen/today")>=2000)//师门次数总数
		{
			return notify_fail("你的扫荡此时已经用完啦。\n");
		}
		if (me->query("shimen/today")<100)//师门次数总数
		{
			return notify_fail("你的扫荡必须手动100次才可以扫荡。\n");
		}
		if (me->query("shimen/today")==0)me->query("shimen/today")==1;
		
		if(me->query("yuanbao")<100)//元宝票限制
			{
          return notify_fail("你的元宝票不足100\n");
         }
	    if (! me->query("family"))
		{
		return notify_fail("你并不属于任何门派，你必须先加入一个\n");
		}
		if (  time()-(int)me->query("shimen/saodang")<3)
		{
			return notify_fail("时间没有过3秒暂时无法扫荡\n");
		}
		
		me->start_busy(1);//一秒忙乱防止多点
		
        //shengyu=(2000-me->query("shimen/today"));//剩余次数为
		
	me->set("shimen/saodang",time());	
	me->add("shimen/today",100);
	me->add("yuanbao",-20);
	me->add("combat_exp",exp*80);
	me->add("potential",pot*80);
	me->add("experience",mar*80);
	me->add("gongxian",80);
	//me->save();//保存
	//TOP_D->add_shimen(me);

	tell_object(me,HIC"你花费20元宝扫荡了100师门获得了\n");
	tell_object(me,HIG+exp*80+"经验\n");
	tell_object(me,HIG+pot*80+"潜能\n");
	tell_object(me,HIG+mar*80+"实战体会\n");
	tell_object(me,HIG+"80师门贡献\n");
	tell_object(me,ZJURL("cmds:saodang yuanbao")ZJSIZE(20)"继续扫荡"NOR+"\n");		
		return  1;			
	}
	if(!arg) 
		{
	tell_object(me,"你要用那种扫荡类型\n");
        return  1;			
	    }

 }	



int help(object me)
{
	write(@HELP
指令格式 : arg  <次数>

这个指令可以让你扫荡师门次数 来获取奖励 但是无法触发物品奖励哦
HELP
	);
	return 1;
}
