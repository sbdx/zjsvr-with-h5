//inventory.c

#include <ansi.h>

inherit F_CLEAN_UP;

int main(object me, string arg)
{
	    object ob,obj;
		string user,hb,yqh,yqc,lop;
		int b,c,h,max,min,j;
        if(!arg)
		return notify_fail("你要领取什么？\n");
	    
	if (sscanf(arg, "%s %s %d",hb,arg,h) != 3)
	return notify_fail("没有这个红包！\n");
		/*
		*hd 红包类型
		*arg 玩家id
		*h  发红包次数
		*/
	user = lower_case(arg);
	if(!(ob=find_player(arg)))
	{
		ob = new(LOGIN_OB);
		ob->set("id",arg);
		ob->set("body", USER_OB);
		ob = LOGIN_D->make_body(ob);
		destruct(ob);
	}
	if(!ob)
	return notify_fail("红包已经领取完！\n");
	
	b=ob->query("hongbao/"+h+"/hbje");//红包金额
	c=ob->query("hongbao/"+h+"/lqcs");//领取次数
	if(c<=0)
	{
		ob->delete("hongbao/"+h+"");
		ob->save();
	return notify_fail("红包已经领取完！\n");
	}
	if(me->query("lingquhb/"+arg+"/"+h+""))//lingqu/红包玩家id/红包次数
	return notify_fail("你已经领取过这个红包，不能再领取！\n");
    ob->add("hongbao/"+h+"/lqcs",-1);//发放红包玩家次数减一
	ob->save();
	me->add("lingquhb/"+arg+"/"+h+"",1);//我增加此次红包领取次数
	max=(b/c)+(b/(c*2));
	min=b/(c*2);
	j=random(max-min)+min;


	if(hb=="元宝")
	{
   
   	if(ob->query("hongbao/"+h+"/lqcs")==0)//红包领取完毕
	  {
	  j=ob->query("hongbao/"+h+"/hbje");//获取一下红包金额
	  ob->add("hongbao/"+h+"/hbje",-j);//红包金额清零
	  ob->save();
	  me->add("yuanbao",j);//返回红包金额？
	  
	  if(ob->query("hongbao/"+h+"/max"))//有运气王的id，加载对象
	  {
		lop=ob->query("hongbao/"+h+"/max");
        yqh = lower_case(lop);
	    if(!(obj=find_player(lop)))
	    {
		obj = new(LOGIN_OB);
		obj->set("id",lop);
		obj->set("body", USER_OB);
		obj = LOGIN_D->make_body(obj);
		destruct(obj);
	    }
		if(j>ob->query("hongbao/"+h+"/maxsz"))//更新运气王
		{
		ob->set("hongbao/"+h+"/max",me->query("id"));
        ob->set("hongbao/"+h+"/maxsz",j);		
	    ob->save();
		}
		if(ob&&obj)
		CHANNEL_D->do_channel(ob,""+ob->query_temp("pindao/id")+"",""HIY"『元宝红包』"HIC"已经领取完，〖"WHT+obj->query("name")+HIC"〗是"MAG"运气王"HIC"，领取到"HIR+ob->query("hongbao/"+h+"/maxsz")+HIC"点元宝!"NOR"\n");
	  }
       else{//没有运气王，把我当做运气王
		ob->set("hongbao/"+h+"/max",me->query("id"));
        ob->set("hongbao/"+h+"/maxsz",j);		
	    ob->save(); 
		if(ob)
		CHANNEL_D->do_channel(ob,""+ob->query_temp("pindao/id")+"",""HIY"『元宝红包』"HIC"已经领取完，〖"WHT+me->query("name")+HIC"〗是"MAG"运气王"HIC"，领取到"HIR+ob->query("hongbao/"+h+"/maxsz")+HIC"点元宝!"NOR"\n");
	   }	  
	}
	else{
	ob->add("hongbao/"+h+"/hbje",-j);//红包总金额减少
	ob->save();
	me->add("yuanbao",j);//我元宝增加
	if(ob->query("hongbao/"+h+"/max"))//存在运气王
	{
	if(j>ob->query("hongbao/"+h+"/maxsz"))//更新运气王
		{
		ob->set("hongbao/"+h+"/max",me->query("id"));
        ob->set("hongbao/"+h+"/maxsz",j);		
	    ob->save();
		}	
	}
	else//不存在把我当做运气王
	{
        ob->set("hongbao/"+h+"/max",me->query("id"));
        ob->set("hongbao/"+h+"/maxsz",j);		
	    ob->save(); 
	}
	}
	tell_object(ob,HIW""+me->query("name")+"领取了你的"HIR"红包"HIW"，获得了"HIY+j+HIW"点元宝"NOR"\n");  
	return notify_fail(HIC"你领取了一个"HIR"红包"HIC"!\n你获得了"HIY+j+HIC"个元宝!\n目前剩余"HIR"红包"HIC"个数"HIY+ob->query("hongbao/"+h+"/lqcs")+HIC"个!\n"HIR"红包"HIC"剩余金额"HIY+ob->query("hongbao/"+h+"/hbje")+HIC"！"NOR"\n");
	}
	else if(hb=="铜板")
	{
	if(ob->query("hongbao/"+h+"/lqcs")==0)
	{
	  j=ob->query("hongbao/"+h+"/hbje");
	  ob->add("hongbao/"+h+"/hbje",-j);
	  ob->save();
	  me->add("balance",j);
	   if(ob->query("hongbao/"+h+"/max"))
	  {
		lop=ob->query("hongbao/"+h+"/max");
        yqh = lower_case(lop);
	    if(!(obj=find_player(lop)))
	    {
		obj = new(LOGIN_OB);
		obj->set("id",lop);
		obj->set("body", USER_OB);
		obj = LOGIN_D->make_body(obj);
		destruct(obj);
	    }
		if(j>ob->query("hongbao/"+h+"/maxsz"))
		{
		ob->set("hongbao/"+h+"/max",me->query("id"));
        ob->set("hongbao/"+h+"/maxsz",j);		
	    ob->save();
		}
		if(ob&&obj)
	CHANNEL_D->do_channel(ob,""+ob->query_temp("pindao/id")+"",""HIY"『铜板红包』"HIC"已经领取完，〖"WHT+obj->query("name")+HIC"〗是"MAG"运气王"HIC"，领取到"HIR+ob->query("hongbao/"+h+"/maxsz")+HIC"枚铜板!"NOR"\n");
	  }
       else{
		ob->set("hongbao/"+h+"/max",me->query("id"));
        ob->set("hongbao/"+h+"/maxsz",j);		
	    ob->save(); 
		if(ob)
	CHANNEL_D->do_channel(ob,""+ob->query_temp("pindao/id")+"",""HIY"『铜板红包』"HIC"已经领取完，〖"WHT+me->query("name")+HIC"〗是"MAG"运气王"HIC"，领取到"HIR+ob->query("hongbao/"+h+"/maxsz")+HIC"枚铜板!"NOR"\n");
	   }
	}
	else{
	ob->add("hongbao/"+h+"/hbje",-j);
	ob->save();
	me->add("balance",j);
		if(ob->query("hongbao/"+h+"/max"))
	{
	if(j>ob->query("hongbao/"+h+"/maxsz"))
		{
		ob->set("hongbao/"+h+"/max",me->query("id"));
        ob->set("hongbao/"+h+"/maxsz",j);		
	    ob->save();
		}
	}
	else
	{
ob->set("hongbao/"+h+"/max",me->query("id"));
        ob->set("hongbao/"+h+"/maxsz",j);		
	    ob->save(); 
	}
	}
	tell_object(ob,HIW""+me->query("name")+"领取了你的"HIR"红包"HIW"，获得了"HIY+j+HIW"枚铜板"NOR"\n");  
	return notify_fail(HIC"你领取了一个"HIR"红包"HIC"!\n你获得了"HIY+j+HIC"枚铜币!\n目前剩余"HIR"红包"HIC"个数"HIY+ob->query("hongbao/"+h+"/lqcs")+HIC"个!\n"HIR"红包"HIC"剩余金额"HIY+ob->query("hongbao/"+h+"/hbje")+HIC"！"NOR"\n");
	}
	
}

int help (object me)
{

	return 1;
}