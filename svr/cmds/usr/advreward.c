//advreward.c
//查看和领取对应的推广报酬
//add by white,2020-05-08

#include <ansi.h>

int main(object me, string arg)
{
	mapping advlist, rewardlist;
	object ob, adv_ob;
	string msg, rev_id,adv_id, tempstr;
	//奖励宝票
	int ireward = 800;

	if (!arg){
		ob = new(LOGIN_OB);
		ob->set("id", me->query("id"));
		//ob->set("id", "wenjian");
		if (!ob->restore())
		{
			destruct(ob);
			tell_object(me, "用户档案出错，请联系管理人员。\n");
			return 1;
		}
		advlist = ob->query("advertise");
		rewardlist = ob->query("rewardlist");

		if (!mapp(advlist) || !sizeof(advlist)){
			msg = ZJOBLONG + "你目前没有完成任何推广，赶快去推广吧！\n";
		}
		else
		{
			msg = ZJOBLONG + "你的推广信息：\n";
			msg += ZJOBACTS2 + ZJMENUF(1, 2, 9, 30);
			foreach(adv_id in keys(advlist))
			{
				adv_ob = new("/clone/user/login");
				adv_ob->set("id", adv_id);
				if (catch (adv_ob->restore()))
				{
					write(sprintf("Login: %s can not be loaded.\n", adv_id));
					continue;
				}
				adv_ob = LOGIN_D->make_body(adv_ob);
				if (!objectp(adv_ob))
				{
					write(sprintf("User: %s can not be make.\n", adv_id));
					continue;
				}

				tempstr = ultrap(adv_ob) ? "【大宗师】" : "【非大宗师】";
				msg += sprintf("%-20s%-20s", "  " + adv_ob->query("name"), tempstr);
				tempstr = "【未领取】";
				if (mapp(rewardlist) && sizeof(rewardlist)){
					foreach(rev_id in keys(rewardlist)){
						if (adv_id == rev_id){
							tempstr = RED"【已领取】"NOR;
							break;
						}
					}
				}
				
				msg += sprintf("%-20s", tempstr);
				msg += ":advreward " + adv_id + ZJSEP;

				destruct(adv_ob);
			}
		}
		msg += "\n";
		write(msg);
	}
	{
		ob = new(LOGIN_OB);
		ob->set("id", me->query("id"));
		//ob->set("id", "wenjian");
		
		if (!ob->restore())
		{
			destruct(ob);
			tell_object(me, "用户档案出错，请联系管理人员。\n");
			return 1;
		}
		advlist = ob->query("advertise");
		rewardlist = ob->query("rewardlist");

		//查看当前人员是否在我的推广名单中
		if (mapp(advlist) && sizeof(advlist)){
			foreach(adv_id in keys(advlist)){
				if (arg == adv_id){
					adv_ob = new("/clone/user/login");
					adv_ob->set("id", adv_id);
					if (catch (adv_ob->restore()))
					{
						write(sprintf("Login: %s can not be loaded.\n", adv_id));
					}
					adv_ob = LOGIN_D->make_body(adv_ob);
					if (!objectp(adv_ob))
					{
						write(sprintf("User: %s can not be make.\n", adv_id));
					}
					//查看当前人员是否满足大宗师条件
					if (ultrap(adv_ob)){
						ob->set("rewardlist/" + arg, time());
						ob->save();
						me->add("yuanbao_2", ireward);
						me->save();
						tell_object(me, "领取完成。\n");
					}
					else
					{
						tell_object(me, "未达到领取条件。\n");
					}
					destruct(ob);
					break;
				}
			}
		}
	}
	return 1;
}

