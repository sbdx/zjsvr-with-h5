inherit ROOM; 
void create_npc();
void action_reward();
void create() 
{ 
        set("short", "通天塔"); 
        set("long", @LONG 
        		这是一座九万九千九百九十九层高的塔楼。
LONG
); 		
        
        
        //不清除数据
        set("cleanup_custom",1);
        //NPC属性比例 1000级开始
        set("npc_scal",80);
        set("quest_count",1);
        set("fuben/room",1);
        set("no_learn",1);
        setup();
        create_npc();
}







//提供给NPC死亡时调用
void cb_func()
{
	tell_room(this_object(),HIY"【系统提示】你已成功通过第"+query("quest_count")+"层.!"NOR"\n");
	
	//先奖励
	action_reward();
	
	//每次按50递增
	add("npc_scal", 1);
	add("quest_count",1);
	
	//NPC死亡后创建NPC
	create_npc();
	
	
}



void single_reward(int count,object crux_user)
{
  
  
  
  
  
		
		if (  count % 50 == 0 ){
			CHANNEL_D->do_channel( this_object(), "rumor", sprintf("听说%s已成功挑战通天塔%d层！",crux_user->query("name"),count) );
		}
		
}





//执行奖励
void action_reward(){
		
	object *obj;
	int count = 0;

	count = query("quest_count");
	obj = filter_array(all_inventory(this_object()),(: userp($1) :) );
	
	if ( sizeof( obj ) >0  ){//有玩家
		single_reward(count,obj[0]);
	}
	

}



//创建NPC
void create_npc(){
	
  object ob;
  int i;
		string *ks;
		
	
			ob = new("/clone/questnpc/fuben_killed","boss");
			
			
			NPC_D->init_attr( ob, query("npc_scal") );
				
			ks = keys( ob->query_skills() );
			for (i = 0; i < sizeof(ks); i++){
				if( query("npc_scal") > 1200  ){
					ob->set_skill(ks[i], 1200 );
				}else{
					ob->set_skill(ks[i], query("npc_scal") );
				}
			}
			i = query("npc_scal");
			ob->set("combat_exp",(i*i*i)/10 );
			set_temp("apply/reduce_busy", 100);
			//添加神兵属性
			if(i > 1)
			{
				 set_temp("apply/def_damage", 80);//反伤
				 set_temp("apply/reduce_damage", 50);//化解伤害
				 set_temp("apply/double_damage", 50);
				 set_temp("apply/reduce_cold", 50);
				 set_temp("apply/reduce_fire", 50);
				 set_temp("apply/reduce_lighting", 50);
				 set_temp("apply/reduce_magic", 50);
				 set_temp("apply/ap_power", 2);
				 set_temp("apply/dp_power", 2);
				 set_temp("apply/da_power", 2);
			}
			
			
			ob->set_skill("martial-cognize",300+random(100) );
			ob->move( this_object() );
		
}










