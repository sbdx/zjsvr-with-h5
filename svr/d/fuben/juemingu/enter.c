inherit ROOM; 
void create_npc();
void action_reward();
void create() 
{ 
        set("short", "绝命谷"); 
        set("long", @LONG 
相传，很多年前有几位绝世高手，由于朝廷追杀，藏身在此，从此不问世事。
多少年来，不知道有过多少江湖豪杰，武林高手来此挑战。最终不得而归。
LONG
); 		
        
        
        //不清除数据
        set("cleanup_custom",1);
        
        //NPC属性比例 300级开始
        set("npc_scal",300);
        set("quest_count",1);
        set("fuben/room",1);
        set("no_learn",1);
        
        set("exits", ([
        		"out" : "/d/city/wumiao",
        		
        	]));
        
        
        
        setup();
      //  replace_program(ROOM);
        //创建一个NPC
        create_npc();
        
        
        
        
        //设置循环
       // set_heart_beat(20);
}







//提供给NPC死亡时调用
void cb_func()
{
	tell_room(this_object(),HIY"【系统提示】你已成功挑战第"+query("quest_count")+"个英雄.!"NOR"\n");
	
	//先奖励
	action_reward();
	
	//每次按50递增
	add("npc_scal", 1);
	add("quest_count",1);
	
	//NPC死亡后创建NPC
	create_npc();
	
	
}



void single_reward(int count,object crux_user)
{
  int coin;
  
  
  
		mapping rwd_items = ([]);
		
		
	
		
		
		coin = 100+random(80);
	
	
		
		if (  count % 50 == 0 ){
			CHANNEL_D->do_channel( this_object(), "rumor", sprintf("听说%s已成功挑战绝命谷第%d个英雄！",crux_user->query("name"),count) );
		}
		
		
		
		GIFT_D->bonus(crux_user,
									([
									  //"exp":exp,
									  //"pot":pot,
									  //"mar":mar,
									  "coin":coin,
									  "items":rwd_items,
									  ])
						);
	

	
}

//团队则按 层数奖励 每层都有奖励
void team_reward(int count, object crux_user)
{
 int pot,exp,mar,team_count;
 
 object  leader,teams;
	
	
	
	leader 		= crux_user->query_leader();
	teams  		= crux_user->query_team();
	team_count 	= sizeof(teams);
	
	//少于2个人 不奖励
	if (team_count <=0 || team_count > 3 ) return;
	
	pot = 100+random(80);//每个人每层奖励基数-潜能
	exp = 100+random(80);//每个人每层奖励基数-经验
	mar = 60+random(80);//每个人每层奖励基数-体会
	
	
	//调整基数的情况 每800层调整基数
	if ( count % 10 == 0 ){
		pot  += pot * count /100 ;
		exp  += exp * count /100 ;
		mar  += mar * count /100 ;
	}
	
	
	


	foreach(object ter in teams)
	{
		if ( !objectp(ter) || !interactive(ter))  continue;
		if ( environment(ter) != this_object()  ) continue;
		
		if ( ter->query("combat_exp") < 4000000 ){
			//防止新人坐高手的车 300级左右才能有收益
			pot /= 5;
			exp /= 5;
			mar /= 5;
		}
		
		GIFT_D->bonus(ter,
									([
									  "exp":exp,
									  "pot":pot,
									  "mar":mar,
									  ])
						);
	}
	
	
	//每100层一次提示
	if (  count%100 == 0 && ( objectp(leader) && interactive(leader) ) )
	{
		CHANNEL_D->do_channel( this_object(), "rumor", sprintf("听说%s的队伍，突破重围，战无不胜，已成功击败绝命谷%d个高手！",leader->query("name"),count) );
	}

}




//执行奖励
void action_reward(){
		
	object *obj;
	int count=0;

	count = query("quest_count");
	obj = filter_array(all_inventory(this_object()),(: userp($1) :) );
	
	if ( sizeof( obj ) >0  ){//有玩家
		if ( sizeof(obj[0]->query_team()) > 0 ){
			
			team_reward(count,obj[0]);
			//single_reward(count,obj[0]);
		}else{
			single_reward(count,obj[0]);
		}
	}
	

}



//创建NPC
void create_npc(){
	
  object ob;
  int i;
		string *ks;
		
		//nc = 3+random(3);
		
		
		
		
		//for(k=0;k < nc;k++){
			ob = new("/clone/questnpc/fuben_killed","juemin-npc");
			
			NPC_D->init_attr( ob, query("npc_scal") );
				
			ks = keys( ob->query_skills() );
			for (i = 0; i < sizeof(ks); i++){
				
				if ( query("quest_count") >= 300 ){
					ob->set_skill(ks[i], 550+random(100) );
				}else{
					ob->set_skill(ks[i], query("npc_scal") );
				}
			}
					
			if( query("quest_count") >= 300 && query("quest_count") < 1000 ){
				ob->set_temp("reborns",1);
			}else if (  query("quest_count") >= 1000 && query("quest_count") < 1500 ){
				ob->set_temp("reborns",2);
			}else if( query("quest_count") >= 1500 && query("quest_count") < 2000 ){
				ob->set_temp("reborns",4);
			}else if( query("quest_count") >= 2000 && query("quest_count") < 2500 ){
				ob->set_temp("reborns",6);
			}else if( query("quest_count") >= 2500 && query("quest_count") < 3000 ){
				ob->set_temp("reborns",8);
			}		
					
						
			
		
			//比例
		   // ob->set("uplevel_func", (: this_object()->uplevel_func :) );
		   // ob->set("cb_object",this_object());
		    //ob->set("cb_func","cb_func");
			ob->set_skill("martial-cognize", 	300+random(100) );
			ob->move( this_object() );
			
		//}
		
		
		
		
	
		
}
/*  由于未知原因 玩家退出副本都摧毁副本会造成下一次进入时报错，所以先未设置退出摧毁
int valid_leave(object me,string dir)
{
	object *players,env;
	object *teams;
	env = environment(me);
	players = all_inventory(env);
	
	teams = filter_array(players,(: userp($1) && living($1)  :));
	if(sizeof(teams)-1 == 0)
	{
	//	destruct(this_object());
		return 1;
	}
	return 1;
}


*/







