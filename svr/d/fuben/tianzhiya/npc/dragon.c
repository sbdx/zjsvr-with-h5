// This program is a part of NT mudlib
// boss 模板

#include <ansi.h>
#define MAX_EXP 2000000000
#define DEBUG 0
//inherit BOSS;
inherit NPC;
int big_blowing();
int roar();
int reward_call(object killer);

void create()
{
        set_name( HIB "镇海神龙" NOR, ({ "sea dragon king", "dragon king", "dragon" }) );
        set("long", HIB "这是一条全身碧绿，吸大海之间的精华而长大"
                    "的神龙！它全身散发着碧绿色的光芒！\n\n" NOR);

        set("race", "野兽");
        set("gender", "雄性");
        set("age", 200);
        set("no_get", 1);
        set("shen_type", 0);
        set("attitude", "aggressive");
        set("limbs", ({ "头部", "身体", "尾巴" }) );
        set("verbs", ({ "bite", "claw" }) );

        set("dex",120);
        set("con",120);
        set("int",120);
        set("str",120);
        set("combat_exp", 500000000);
        set("level", 70);
        set("qi", 2000000);
        set("max_qi", 2000000);
        set("jing", 1000000);
        set("max_jing", 1000000);
        set("neili", 4000000);
        set("max_neili", 4000000);
        set("neili", 4000000);
        set("jiali", 20000);

        set("unarmed", 1500);
        set("parry", 1500);
        set("claw", 1500);
        set("dodge", 1500);
        set("force", 1500);
        //杀手最小经验 低于这个经验奖励减少
		set("killer_sm_exp",50000000),
		//大于800级只出材料和黄金
		set("killer_max_exp",50000000),
		
		
		
		
        
        set("no_nuoyi",1);
        set("chat_chance", 5);
        set("chat_msg", ({
                HIB "海底打来一个巨浪！差点把你卷走！\n" NOR,
                HIB "镇海神龙突然吐出一颗湛蓝的珠子，又卷了回去！\n" NOR,
        }));

        //此数大过120则按end_time时间randmove;
        //此书越大，NPC自动移动的机率越大
         set("chat_chance", 80);
         set("chat_msg", ({
                     (: random_move :)
          }));
     	
        
        set("chat_chance_combat", 100);
        set("chat_msg_combat", ({
               (: big_blowing :),
        }));
        set("end_time", 120);
        
#if  DEBUG
        set("dex",10);
        set("con",10);
        set("int",10);
        set("str",10);
        set("combat_exp", 2000);
        set("level", 70);
        set("qi", 100);//2000000
        set("max_qi", 100);
        set("jing", 100);
        set("max_jing", 100);
        set("neili", 100);
        set("max_neili", 100);
        set("neili", 100);
        set("jiali", 100);

        set("unarmed", 100);
        set("parry", 100);
        set("claw", 100);
        set("dodge", 100);
        set("force", 100);
		
        set("end_time", 80);
        set("reward_call", (:reward_call:));
        
        
        set("chat_chance_combat", 100);
        set("chat_msg_combat", ({
                   
        }));
        
        
#endif
        

       
        

        set("rewards", ([
                "exp"   : 50000,
                "pot"   : 500000,
                "mar"   : 100000,
                "score" : 200,
                "gold"  : 500,
                "weiwang"   : 500,
        ]));
        
        
        set("drops", ([
                      
                      	"FI&/clone/enchase/gem/rune11" :    50,
                      	"FI&/clone/enchase/gem/rune12" :    50,
                      	"FI&/clone/enchase/gem/rune13" :    50,
                      	"FI&/clone/enchase/gem/rune14" :    50,
                      	"FI&/clone/enchase/gem/rune15" :    50,
                    	"FI&/clone/enchase/gem/rune16" :   5,
                      	"FI&/clone/enchase/gem/rune17" :   5,
                      	"FI&/clone/enchase/gem/rune18" :   5,
                      	"FI&/clone/enchase/gem/rune19" :   5,
                      	"FI&/clone/enchase/gem/rune20" :   5,
                      	"FI&/clone/enchase/gem/skull2" :    5,
                      	"FI&/clone/enchase/gem/skull3" :    5,
                      	"FI&/clone/enchase/gem/skull4" :    5,
                        "FI&/clone/gift/jinkuai" :    50,
                        "FI&/clone/gift/tianxiang" :  50,
                        "FI&/clone/shop/tianxiang" :  50,
                        "FI&/clone/gift/xianxing" :   50,
                        "FI&/clone/gift/xisuidan" :   50,
                        "FI&/clone/shop/xidiandan" :   5,
                        "FI&/clone/medicine/biantaidan" :   5,
                        "FI&/clone/medicine/baoming" :   5,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,
						"FI&/clone/shop/putao" :   100,

        ]));
        
        
    
        
    
        setup();
        set_temp("born_time", time());

        set_temp("apply/attack", 5000+random(1000));
        set_temp("apply/unarmed_damage",5000+random(1000));
        set_temp("apply/armor", 2000+random(1000));
        set_temp("apply/add_weak", 60);
       // set_temp("apply/reduce_damage", 80);
        set_temp("apply/reduce_busy", 20);
        set_temp("apply/add_busy", 3);
        set_temp("apply/add_poison", 80);
        set_temp("hurting_damage",1000);
}


varargs void start_busy(mixed new_busy, mixed new_interrupt)
{
        return;
}

int reward_call(object killer){
	
		object env,*obs,me;
		int n,percent;
		mapping rewards,drops;
		me = this_object();
		
        if( !(rewards = query("rewards")) ) {
        	  	destruct(this_object());
                return 1;
        }

        
        if( mapp(drops = query("drops")) ) {
                EQUIPMENT_D->killer_reward(killer, this_object(), drops);
        } 
        
        
		
	    if( objectp(killer) ) {
	    	//有了杀死我的人，继续查找其他杀手
	            env = environment(me);
	            //obs = sizeof( query_killer() ) > 0 ? query_killer() : ({ killer });
	            //保证杀手和团队的人员在一起
	            obs = filter_array(all_inventory(env), (: userp($1) && (environment($1) == $2) && is_killing($1->query("id")) :),env);
	        
	            
	            //计算多少人参与战斗
	            n = sizeof(obs);
	            percent = 100 / n;
	            //分配奖励
	            foreach( object user in obs ) {
	                    if( !objectp(user) ) continue;
	                  
	                    if( user->query("combat_exp") > MAX_EXP ){
	                          
	                    	tell_object(user, ((killer == user)?"您":killer->name())+"杀死"+query("name")+"，但你的经验已经超过极限,没有奖励\n");
	                    	continue;
	                    }else if( user->query("combat_exp") > me->query("killer_max_exp") ){
	                    	//玩家大于800级
	                    	//percent = 8+random(12);
	                    	rewards["exp"] -=  rewards["exp"] * 98 / 100;
	                    	//提高潜能
	                    	rewards["pot"] -=  rewards["exp"] * 95 / 100;
	                    }else{
	                    	rewards["gold"] -=  rewards["gold"] * 95 / 100;
	                    }
	                    
	                
	                    GIFT_D->bonus(user, rewards);
	            }
	            
	            
	          
	
	    }
	    
		destruct( this_object() );
	    
}


int roar(){
	
	
    int dam, i , num;
        object *inv;
        

        message_vision(HIB "$N" HIY "突然腾空而起，一声龙啸，全身发出耀眼"
                           "的金色。！！"NOR"\n" , this_object());
    	

        inv = all_inventory(environment(this_object()));

        num = sizeof(inv);
        if (num < 1) num = 1;
        if (num > 2) num = 2;

        dam =  500+random(500);

        for (i=sizeof(inv)-1; i>=0; i--)
        {
            //直接伤害
        	inv[i]->receive_damage("qi", dam, this_object());
            inv[i]->receive_damage("jing", dam/2, this_object());
            //伤害有效精
            inv[i]->receive_wound("qi", dam, this_object());
            inv[i]->receive_wound("jing", dam/2, this_object());
            COMBAT_D->report_status(inv[i], 1);
             
        }
        return 1;
	
	
	
	
}

int big_blowing()
{
        message_vision(HIB "$N" HIB "从海水里深深地吸入一口气，全身发出耀眼"
                       "的蓝色，整个龙腹胀大了几倍！！\n" NOR, this_object());

        remove_call_out("hurting");
        call_out("hurting", random(2) + 1);
        return 1;
}

int hurting()
{
        int dam, i , num;
        object *inv;
        
        message_vision(HIB "$N" HIB "吐出一股巨大的海浪．．．．．．整个天地似乎都被吞没！！！\n" NOR,
                       this_object());

        inv = all_inventory(environment(this_object()));

        num = sizeof(inv);
        if (num < 1) num = 1;
        if (num > 5) num = 5;

        dam =  query_temp("hurting_damage") / num;

        for (i=sizeof(inv)-1; i>=0; i--)
        {
                if (living(inv[i]) && inv[i] != this_object())
                {
                       // dam-=query_temp("apply/reduce_cold", inv[i]);
					       dam-=inv[i]->query_temp("apply/reduce_cold");
                        if (dam <0) dam = 0;

                        inv[i]->receive_damage("qi", dam, this_object());
                        inv[i]->receive_damage("jing", dam/2, this_object());
                        inv[i]->receive_wound("qi", dam, this_object());
                        inv[i]->receive_wound("jing", dam/2, this_object());
                        COMBAT_D->report_status(inv[i], 1);
                }
        }
        return 1;
}

mixed hit_ob(object me, object ob, int damage_bouns)
{
        int damage;
        int reduce;
     
        damage = 5000 + random(5000);
       // reduce=query_temp("apply/reduce_cold", ob);
	    reduce=ob->query_temp("apply/reduce_cold");
        damage = damage - damage * reduce / 100;
        if (damage < 0) damage = 0;
        ob->receive_damage("qi", damage, me);
        ob->receive_wound("qi", damage, me);
        me->set("neili", me->query("max_neili"));
        return HIB "$N" HIB "“哈”的一声吐出一口寒气，登时令$n"
               HIB "全身几乎冻僵。\n" NOR;
}


void random_move()
{
        if (time() - query_temp("born_time") > query("end_time"))
        {
                message_vision("$N长啸一声，响彻天地，然后身体慢慢变得透明，消失在圣湖之中。\n", this_object());
                CHANNEL_D->do_channel(this_object(), "rumor",
                        "听说" + name() + HIM "在人间走了一遭后，百无聊奈，又返回了圣湖。" NOR"\n");
                destruct(this_object());
                return;
        }
      
}



