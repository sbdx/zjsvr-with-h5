#include <ansi.h>

inherit NPC;

//inherit BOSS;

void create()
{
        set_name(HIM "樱花剑客" NOR, ({ "boss",}));
        set("title", HIW "来自扶桑樱花之村的" NOR);
        set("gender", "无性");
        set("age", 22);
        set("long", @LONG
这是一位来自扶桑的剑客，白色的长袍上绣有樱花的标记。
LONG);
        set("attitude", "friendly");
        set("str", 150);
        set("int", 150);
        set("con", 150);
        set("dex", 60);
        set("per", 150);
        set("shen_type", 0);

        set("qi", 100000);
        set("max_qi", 100000);
        set("no_nuoyi", 1);//不被大挪移

        set("jing", 100000);
        set("max_jing", 100000);
        set("jingli", 100000); 
        set("max_jingli", 100000); 

        set("neili", 50000); 
        set("max_neili", 50000); 
        set("jiali", 1200); 
        set("combat_exp", 20000000);

	
        set("special_skill/ghost", 1);
	
        set_skill("force", 1000);
        set_skill("pixie-jian", 800);
        set_skill("surge-force", 800);
        set_skill("six-finger", 800);
        set_skill("sword", 800);
        set_skill("finger", 800);
        set_skill("whip", 800);
        set_skill("dodge", 800);
        set_skill("parry", 800);
        set_skill("unarmed", 800);
        set_skill("strike", 800);
        set_skill("zuoyou-hubo", 500);
        set_skill("literate", 1000);
        set_skill("throwing", 800);
       
        set_skill("jingluo-xue", 500);
        set_skill("martial-cognize", 700);

        map_skill("force", "surge-force");
        map_skill("sword", "pixie-jian");
        map_skill("dodge", "pixie-jian");
        map_skill("parry", "pixie-jian");
        map_skill("unarmed", "pixie-jian");
        map_skill("finger", "six-finger");

        prepare_skill("finger", "six-finger");

        set_temp("apply/attack",1000);
        set_temp("apply/defense", 1000);
        set_temp("apply/damage", 1000);
        set_temp("apply/unarmed_damage", 1000);
        set_temp("apply/armor", 2000);
        
        
        set_temp("apply/reduce_damage", 80);
        set_temp("apply/reduce_busy", 36);
        set_temp("apply/add_busy", 3);
        set_temp("apply/add_poison", 80);
        
       
        set("end_time", 120);
        set("chat_chance_combat", 30);
        //此数大过120则按end_time时间randmove;
        set("chat_chance", 80);
        
        set("chat_msg", ({
                    (: random_move :)
         }));
        
        set("chat_msg_combat", ({
                (: command("perform sword.duo and sword.kuihua") :),
                (: command("perform sword.pi and sword.cimu") :),
                (: exert_function, "recover" :),
                (: exert_function, "powerup" :),
                (: perform_action, "perform sword.kuihua twice" :),
                (: perform_action, "perform finger.qi twice" :),
        }) );

        
        set("rewards", ([
                      "exp"   : 10000,
                      "pot"   : 50000,
                      "mar"   : 50000,
                      "score" : 50,
                      "gold"  : 100,
                      "weiwang"   : 20,
              ]));
        
        set("drops", ([
                               	"FI&/clone/enchase/gem/blugem1" :   5000,
                               	"FI&/clone/enchase/gem/grngem1" :   5000,
                               	"FI&/clone/enchase/gem/maggem1" :   5000,
                               	"FI&/clone/enchase/gem/redgem1" :   5000,
                               	"FI&/clone/enchase/gem/yelgem1" :   5000,
								"FI&/clone/shop/putao" :   20000,
								"FI&/clone/shop/putao" :   20000,
								"FI&/clone/shop/putao" :   20000,
								"FI&/clone/shop/putao" :   20000,
								"FI&/clone/shop/putao" :   20000,
								"FI&/clone/shop/putao" :   20000,
								"FI&/clone/shop/putao" :   20000,


         ]));
        setup();
        set_temp("born_time", time());

        carry_object("/clone/weapon/changjian")->wield();
        carry_object("/clone/cloth/baipao")->wear();
}



void random_move()
{
        if (time() - query_temp("born_time") > query("end_time"))
        {
                message_vision("$N打了哈欠。百无聊赖的走了\n", this_object());
                CHANNEL_D->do_channel(this_object(), "rumor",
                        "听说" + name() + HIM "在中原找不到对手，回到了他的故乡。" NOR);
                destruct(this_object());
                return;
        }
      
}



