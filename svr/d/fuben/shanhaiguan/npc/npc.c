// sb7.c 

//inherit BOSS;
inherit NPC;


void create()
{
	set_name("飞天大盗", ({ "boss"}));
	set("gender", "男性");
    set("title", "飞天大盗");
	set("age", 30);
	set("combat_exp", 100000+random(500));
	set("str", 29);
	set("dex", 28);
	set("con", 30);
	set("int", 30);
	set("no_nuoyi", 1);
	set("auto_perform",1);
	set("neili", 600+random(200)); 
	set("max_neili", 600+random(200));
	set("max_qi", 1000+random(200));
	
	set("max_jing", 1000);
	set("jiali", 50);
	set_skill("blade",120);
	set_skill("parry", 120);
	set_skill("dodge", 120);
	set_skill("force", 120);
	set_skill("wuhu-duanmendao", 120);
	set_skill("lingbo-weibu", 120);
	set_skill("literate", 120);
	set_skill("longxiang", 120);
	
	map_skill("blade", "wuhu-duanmendao");
	map_skill("force", "longxiang");
	map_skill("dodge", "lingbo-weibu");
	
	set_temp("apply/attack", 50);
	set_temp("apply/defense", 100);
	set_temp("apply/damage", 100);
    set_temp("apply/add_weak", 5);
    set_temp("apply/reduce_damage", 20);
    set_temp("apply/reduce_busy", 20);
    set_temp("apply/add_busy",2);
    set_temp("apply/add_poison", 10);
	
    set("rewards", ([
            "exp"   : 1000,
            "pot"   : 1000,
            "mar"   : 1000,
            "score" : 100,
            "gold"  : 10,
            "weiwang"   : 10,
    ]));
    
    
    set("drops", ([
                    
                    "FI&/clone/misc/tongrensuipian" :   10000,
                    "FI&/clone/enchase/gem/blugem1" :   10000,
					"FI&/clone/enchase/gem/blugem1" :   5000,
					"FI&/clone/enchase/gem/grngem1" :   5000,
					"FI&/clone/enchase/gem/maggem1" :   5000,
					"FI&/clone/enchase/gem/redgem1" :   5000,
					"FI&/clone/enchase/gem/yelgem1" :   5000,
					"FI&/clone/shop/putao" :   20000,
					"FI&/clone/shop/putao" :   20000,
					"FI&/clone/shop/putao" :   20000,

            ]));
    //此数大过120则按end_time时间randmove;
    set("chat_chance", 80);
    set("chat_msg", ({
                (: random_move :)
     }));
	
	set("end_time", 120);
	setup();
	set_temp("born_time", time());
	carry_object("/clone/weapon/blade")->wield();

}






void random_move()
{
        if (time() - query_temp("born_time") > query("end_time"))
        {
                message_vision("$N在城中偷盗一翻，提着包裹得意洋洋的走了。\n", this_object());
                CHANNEL_D->do_channel(this_object(), "rumor",
                        "听说" + name() + HIM "在城中偷盗得手，得意的跑了。" NOR);
                destruct(this_object());
                return;
        }
      
}




