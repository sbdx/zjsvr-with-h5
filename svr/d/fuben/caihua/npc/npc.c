// sb7.c 

//inherit FUBEN_BOSS;
inherit NPC;


string *npc_nams = ({
	"老贼",
	"淫贼",
	"新手",
	"飞贼",
	"老王",
	"老李",
	"喽啰",
	"头目",
	"大当家",
	"信陵君",
});


void create()
{
	set_name("采花"+npc_nams[sizeof(npc_nams)-1], ({ "boss", "boss" }));
	set("gender", "男性");
    set("title", "采花大盗");
	set("age", 40);
	set("combat_exp", 6400);
	set("str", 29);
	set("dex", 28);
	set("con", 30);
	set("int", 30);
	set("no_nuoyi", 1);
	set("neili", 200); 
	set("max_neili", 200);
	set("max_qi", 200+random(100));
	
	set("max_jing", 1000);
	set("jiali", 50);
	set_skill("blade",40);
	set_skill("parry", 40);
	set_skill("dodge", 40);
	set_skill("force", 40);
	set_skill("literate",40);
	
	map_skill("blade", "wuhu-duanmendao");
	
	set_temp("apply/attack", 20);
	set_temp("apply/defense", 20);
	set_temp("apply/damage", 20);
	
	set("rewards", ([
	                "exp"   : 50+random(50),
	                "pot"   : 20+random(20),
	]));
	
	
	/*  set("drops", ([
	                      
              	"FI&/clone/medicine/jingqi" :    	50,
              	"FI&/d/city/obj/jinchuang" :    	50,
              	"FI&/clone/gift/cagate" :    		50,
              	"FI&/clone/gift/ccrystal" :    		50,
              	"FI&/clone/misc/tongrensuipian" :   50,
      ]));*/
	
	  
	setup();
	carry_object("/clone/weapon/blade")->wield();

}






