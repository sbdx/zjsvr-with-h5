#include <ansi.h>
inherit ROOM;

int rand_num(int min,int max);

//private void special_bonus(object me, mixed arg)
//{
//	string *ob_list = ({
//		"/clone/gift/xiandan",
//		"/clone/gift/shenliwan",
//		"/clone/gift/unknowdan",
//		"/clone/gift/xisuidan",
//		"/d/item/obj/hantie",
//		"/d/item/obj/wujins",
//		"/d/item/obj/butian",
//		"/d/item/obj/tiancs",
//		"/clone/gift/jinkuai",
//		"/clone/enchase/gem/blugem4",
//		"/clone/enchase/gem/grngem4",
//		"/clone/enchase/gem/maggem4",
//		"/clone/enchase/gem/redgem4",
//		});
//	object ob;
//	//int gongxian;
//
//
//
//	if (stringp(arg))
//		ob = new(arg);
//	else
//		ob = new(ob_list[random(sizeof(ob_list))]);
//
//
//
//	if (!objectp(ob))
//	{
//		tell_object(me, HIC "奖励物品加载失败!请联系巫师!"NOR"\n");
//		tell_object(me, HIC "物品地址为!"NOR"\n");
//		tell_object(me, HIC + arg + NOR"\n");
//		return;
//	}
//
//	if (me) {
//		message_vision("$N从宝箱中找到了一件道具\n", me);
//	}
//
//
//	ob->move(me, 1);
//	tell_object(me, HIM "你获得了一" + ob->query("unit") + ob->name() + HIM "。"NOR"\n");
//}

void create()
{
	set("short", "土匹斯");
	set("long", "相传，各路强盗王将自己的宝物藏于此地。\n");
	set("exits", ([
		"down":"d/city/guangchang",	
	]));
	set("count", 1);
	set("item_desc", ([
		"【宝箱】":"通常用来置放珍贵物品的华丽箱子\n"ZJOBACTS2 + ZJMENUF(4, 4, 9, 32)"打开:open box\n",
	]));
	set("fuben/room",1);
	set_heart_beat(1);
	setup();
}
void init()
{
	add_action("do_open", "open");
}

int do_open(string arg) 
{
	object me;
	int exp; //奖励的经验
	int pot; //奖励的潜能
	int coin,yuanbao; //奖励的钱
	
  //奖励的道具


		mapping ob_list =
				 ([

				              	"FI&/d/item/obj/hantie" :    	50,
				              	"FI&/d/item/obj/wujins" :    	50,
				              	"FI&/d/item/obj/butian" :    		50,
				              	"FI&/d/item/obj/tiancs" :    		50,
				              	"FI&/clone/enchase/gem/blugem4" :   50,
								"FI&/clone/enchase/gem/blugem4" :   50,
								"FI&/clone/medicine/jingqi" :    	50,
								"FI&/d/city/obj/jinchuang" :    	50,
								"FI&/clone/gift/cagate" :    		50,
								"FI&/clone/gift/ccrystal" :    		50,
								"FI&/clone/misc/tongrensuipian" :   50,
				      ]);



		int task_count=0;


	me = this_player();


	if (query("count") < 1) {
		return 0;
	}

	if (arg != "box")
	{
		return notify_fail("你要打开什么？\n");
	}

	if (random(10) < 8 )
	{		
//		string *reawrd_items = ({
//			"/clone/gift/xiandan",
//			"/clone/gift/shenliwan",
//			"/clone/gift/unknowdan",
//			"/clone/gift/xisuidan",
//			"/d/item/obj/hantie",
//			"/d/item/obj/wujins",
//			"/d/item/obj/butian",
//			"/d/item/obj/tiancs",
//			"/clone/gift/jinkuai",
//			"/clone/enchase/gem/blugem4",
//			"/clone/enchase/gem/grngem4",
//			"/clone/enchase/gem/maggem4",
//			"/clone/enchase/gem/redgem4",
//
//			});
//
//		special = reawrd_items[random(sizeof(reawrd_items) - 1)];
//		special_bonus(me, special);


		 EQUIPMENT_D->killer_reward(me, this_object(), ob_list);

	}

	exp = 1000+random(2000);
	pot = 5000+random(2000);
	coin = 80+random(500);
	if(random(100)<=10)
	{
		//奖励任务次数？？副本的？
		task_count = random(30);
		if(random(100)<= 10) task_count+=random(20);
		if(random(100)<= 5 ) task_count+=random(30);
		if(random(100)== 1 ) task_count+=random(60);
	}

	if( random(200) <= 10 ){
		//奖励任务次数？？副本的？
		yuanbao = 1 + random(3);
	}

	GIFT_D->bonus(me, (["exp":exp, "pot" : pot, 
		"coin" : coin, "task_count":task_count,"yuanbao":yuanbao,
	])
	);
	add("count", -1);
	CHANNEL_D->do_channel( this_object(),"chat",sprintf("%s通过不懈努力，在某处获得了宝藏!%s\n",me->query("name"),task_count>0? "额外获得了"+task_count+"成就点数":"!" ));
	
	tell_object(me, "你成功打开宝箱！\n");
	
	return 1;

}


/*int rand_num(int min, int max)
{
	int n;
	n = min + rand() % (max - min)+ min;
	return n;
}*/