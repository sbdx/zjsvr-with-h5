// turou.c ║В╔Н╣и╚Р

inherit ITEM;
inherit F_FOOD;

void create()
{
	set_name("║В╔Н╣и╚Р", ({"gou rou", "gou"}));
	set_weight(300);
	if (clonep())
		set_default_object(__FILE__);
	else {
		set("long", "м╗═в¤с┼у┼ух─║В╔Н╣и╚Р\n");
		set("unit", "═в");
		set("value", 200);
		set("food_remaining", 4);
		set("food_supply", 50);
	}
}

