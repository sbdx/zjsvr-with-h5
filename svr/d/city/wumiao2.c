// Room: /city/wumiao2.c

#include <room.h>

inherit ROOM;
string look_shu();
int ask_character(string arg);
mapping list = ([
	"hanxing-bada" : 3500,
	"yunlong-bian" : 3500,
	"yunlong-shenfa" : 3500,
	"xuanming-zhang" : 4500,
	"hanbing-mianzhang" : 4500,
	"sougu" : 4500,
	"qishang-quan" : 5000,
    "zhuihun-jian" : 6000,
    "baihua-quan" : 9000,
	"shenzhaojing" : 10000,
	
	
]);
mapping lists = ([
	"clone/vip/vip2/book_wuliang.c":7,
	"clone/vip/vip2/dan_jiuzhuan.c":400,
	"clone/vip/vip2/dan_xixin.c" :1000,
	"clone/vip/vip2/tianxingdan.c":1000,



]);

void create()
{
	set("short", "武庙二楼");
	set("long", @LONG
这里是岳王庙的二楼，这里供的是岳飞的长子岳云和义子张宪，两尊塑像
金盔银铠，英气勃勃。
LONG );

	set("no_fight", 1);
	set("no_steal", 1);
	set("no_beg", 1);
	set("no_sleep_room", 1);
	set("exits", ([
		"south" : __DIR__"wumiao",
	]));
		set("action_list", ([
		"更换性格" : "day_sign",
		"师门兑换" : "duihuan",
	]));
	setup();
}
void init()
{
	add_action("ask_character","day_sign");
	add_action("ask_duihuan_skill","duihuan_skill1");
	add_action("do_duihuan_skill","duihuan_skill");
	add_action("ask_duihuan_item","duihuan_item1");
	add_action("do_duihuan_item","duihuan_item");
	add_action("do_duihuan","duihuan");
	
	
}
int do_duihuan()
{
	string pops;
	pops= ZJOBLONG"你要用师门贡献兑换什么？\n";
	pops+=ZJOBACTS2+ZJMENUF(4,4,9,32);
	pops += "物品:duihuan_item1"+ZJSEP;
	pops += "技能:duihuan_skill1"+ZJSEP;
	write(pops+"\n");
	return 1;

}
int ask_duihuan_item()
{
	int i;
	string str,*items;
	object me = this_player();

	str = ZJOBLONG"你目前师门贡献有还有"+me->query("gongxian")+"点，请选择兑换物品：\n"ZJOBACTS2+ZJMENUF(3,3,9,30);
	items = keys(lists);
	for(i=0;i<sizeof(items);i++)
	{
		str += items[i]->query("name")+ZJBR"师门贡献："+lists[items[i]]+":duihuan_item "+items[i]+ZJSEP;
	}
	tell_object(me,str+"\n");
	return 1;
}

int do_duihuan_item(string arg)
{
 int num;
 
	object ob,me = this_player();

	if(!arg)
	{
		tell_object(me,"你要兑换什么物品？\n");
		return 1;
	}

	if(sscanf(arg,"%d %s",num,arg)!=2)
	{
		if(arg->query_amount())
		{
			tell_object(me,INPUTTXT("你想兑换多少"+arg->query("name")+"？","duihuan_item $txt# "+arg+"\n"));
			return 1;
		}
		num = 1;
	}

	if(!lists[arg])
	{
		tell_object(me,"你要兑换什么物品？？\n");
		return 1;
	}

	if(!me->query("gongxian")||me->query("gongxian")<lists[arg]*num)
	{
		tell_object(me,"你的师门贡献不足！！\n");
		return 1;
	}

	me->add("gongxian",-lists[arg]*num);
	ob = new(arg);
	if(ob->query_amount())
		ob->set_amount(num);
	ob->move(me);
	me->save();
	tell_object(me,"你用师门贡献了兑换了"+ob->short()+"。\n");
	return 1;
}

int ask_duihuan_skill()
{
	int i;
	string str,*items;
	object me = this_player();

	str = ZJOBLONG"你目前的师门贡献还有"+me->query("gongxian")+"点，请选择兑换技能：\n"ZJOBACTS2+ZJMENUF(3,3,9,30);
	items = keys(list);
	for(i=0;i<sizeof(items);i++)
	{
		str += to_chinese(items[i])+ZJBR"贡献："+list[items[i]]+":duihuan_skill "+items[i]+ZJSEP;
	}
	tell_object(me,str+"\n");
	return 1;
}

int do_duihuan_skill(string arg)
{
 
 
 object me = this_player();

	if(!arg)
	{
		tell_object(me,"你要兑换什么物品？\n");
		return 1;
	}

	if (!SKILL_D(arg)->valid_learn(me)){
	   tell_object(me,"你不符合技能"+to_chinese(arg)+"的学习条件，无法兑换\n");
		return 1;
	}
	if(me->query_skill(arg,1))
		{
			tell_object(me,"你选择的技能"+to_chinese(arg)+"，你已经会了！\n");
			return 1;
		}
			
			
	

	if(!me->query("gongxian")||me->query("gongxian")<list[arg])
	{
		tell_object(me,"你的师门贡献不足！！\n");
		return 1;
	}

	me->add("gongxian",-list[arg]);
	me->set_skill(arg,50);
	me->save();
	tell_object(me,"你获得了50级的"+to_chinese(arg)+"。\n");
	return 1;
}


int ask_character(string arg)
{
	object me = this_player();
	string msg, type, cs;

	if (me->query("combat_exp")>200000)
		return notify_fail("经验到达20w了，无法再更换性格。\n");
		
	if (!arg || arg == 0) {
		msg = ZJOBLONG+
		      "心狠手辣，宗师心法-九阴神功，提高攻击"ZJBR
			"光明磊落，宗师心法-南海玄功，增加防御"ZJBR
			"狡黠多变，宗师心法-不败神功，提高命中"ZJBR
			"阴险奸诈，宗师心法-葵花魔功，增加闪避"ZJBR
			"请选择你的性格：\n";
		
		msg += ZJOBACTS2+ZJMENUF(2,2,9,30);
		msg += "光明磊落:day_sign 光明磊落";
		msg += ZJSEP"狡黠多变:day_sign 狡黠多变";
		msg += ZJSEP"阴险奸诈:day_sign 阴险奸诈";
		msg += ZJSEP"心狠手辣:day_sign 心狠手辣";
		
		write(msg + "\n");
		
	} else {
		if(sscanf(arg,"%s %s", type, cs) == 2)
		{
			if (me->query("character") == type) {
				tell_object(me, "你本来就是这种性格。\n");
				return 1;
			}

			if (cs == "yes") {
				if (type == "光明磊落")
				 {
					me->set("character", type);
					tell_object(me, "你成功将性格改变为"+type+"。\n");
				} 
			   if (type == "心狠手辣") 
			   {
					me->set("character", type);
					tell_object(me, "你成功将性格改变为"+type+"。\n");
				} 
				if (type == "阴险奸诈")
				 {
					me->set("character", type);
					tell_object(me, "你成功将性格改变为"+type+"。\n");
				} 			
				if (type == "狡黠多变")
				 {
					me->set("character", type);
					tell_object(me, "你成功将性格改变为"+type+"。\n");
				} 							
			} 
		} else {
			msg = ZJOBLONG+"你确定选择性格为"+HIG+arg+NOR"吗？\n";			
			msg += ZJOBACTS2+ZJMENUF(2,2,9,30);
			msg += "确定:day_sign "+arg + " yes";
			msg += ZJSEP"取消: ";
			
			write(msg + "\n");
		}
	}
	return 1;
}