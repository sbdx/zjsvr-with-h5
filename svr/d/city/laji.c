// Room: /city/laji.c
// by 雨萌

inherit ROOM;

void create()
{
	set("short", "垃圾场");
	set("long", @LONG
这是垃圾场，巫师专门为玩家建设的，以便丢弃不需要的东西
LONG );
	set("no_fight", 1);     
	set("exits", ([		
	    "northeast" : __DIR__"guangchang",
	]));
	setup();
}
void init()
{
	add_action("do_drop", "drop");
}

int do_drop(string arg)
{
	object me,obj;
	me=this_player();
	if (! arg)
		return notify_fail("你要丢下什么东西？\n");

	if (! objectp(obj = present(arg, me)))
		return notify_fail("你身上没有这样东西。\n");


	tell_object(me,"你把"+obj->short()+"丢进了垃圾场。\n");
	destruct(obj);
	return 1;
}