// Room: /city/nandajie1.c
inherit ROOM;

void create()
{
	set("short", "皇宫");
	set("long", @LONG
这里便是白云城城主所在。不意外，一片冷清。你感受到阵阵孤独。
LONG );
	set("outdoors", "baiyun");
	set("exits", ([
		"south" : __DIR__"nandajie2",
		"north" : __DIR__"guangchang",
	]));



	replace_program(ROOM);
}

