// Room: /city/dongmen.c
inherit ROOM;

string look_gaoshi();

void create()
{
	set("short", "城墙");
	set("long", @LONG
这里是白云城的城墙。青砖上血锈迹剑痕，述说着它的故事。
LONG );
	set("outdoors", "baiyun");

	set("exits", ([

		"west" : __DIR__"dongdajie2",
	]));


	setup();
}

string look_gaoshi()
{
	return FINGER_D->get_killer() + "\n扬州知府\n程药发\n";
}

