// Room: /city/nanmen.c
inherit ROOM;

string look_gaoshi();

void create()
{
	set("short", "皇宫后花园");
	set("long", @LONG
这里很久以前开满了无数野花。不知道什么时候起，花落了一次。再也没有开过。再往南就是禁地了，不过除了特定的几人谁也不知道入口在哪里，里面有什么。
LONG );
	set("outdoors", "baiyun");


	set("exits", ([
		"north" : __DIR__"nandajie2",
	]));

	setup();
}

string look_gaoshi()
{
	return FINGER_D->get_killer() + "\n扬州知府\n程药发\n";
}

