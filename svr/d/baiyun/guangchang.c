// Room: /city/dongdajie1.c
// YZC 1995/12/04 

inherit ROOM;

void create()
{
	set("short", "城中心");
	set("long", @LONG
这里是白云城的正中心，一个很宽阔的广场，铺着青石地面。
一片肃静。
LONG );

	set("outdoors", "baiyun");
	set("objects", ([

		CLASS_D("baiyun") + "/baixue" : 1,

	]));

	set("exits", ([

		"east" : __DIR__"dongdajie1",
		"south" : __DIR__"nandajie1",
		"west" : __DIR__"xidajie1",
		"north" : __DIR__"beidajie1",
	]));
	
	setup();
}

