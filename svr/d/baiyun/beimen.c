// Room: /city/beimen.c
#include <room.h>

inherit ROOM;


void create()
{
	set("short", "白云城门");
	set("long", @LONG
 悠悠天地几惊心，诺大空城唯白云。在走就是白云城了，
LONG );

	set("exits", ([
		"south" : __DIR__"beidajie2",
	]));
	
	set("outdoors", "baiyun");

}



