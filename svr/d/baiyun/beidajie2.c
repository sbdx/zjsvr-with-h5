// Room: /city/beidajie2.c

inherit ROOM;

void create()
{
	set("short", "北大街");
	set("long", @LONG
这是一条宽阔的青石街道，向南北两头延伸。北边是北城门通向城外。
LONG );
	set("outdoors", "baiyun");
	set("exits", ([
		"south" : __DIR__"beidajie1",
		"north" : __DIR__"beimen",
	]));

	replace_program(ROOM);
}

