// Room: /city/nandajie2.c
// YZC 1995/12/04 
// CLEANSWORD 1996/2/2

inherit ROOM;

void create()
{
	set("short", "皇宫");
	set("long", @LONG
依旧冰，寒。仿佛这里存在的意义便是为了孤独..孤独..只是为了这个人吧。。
LONG );
	set("outdoors", "baiyun");
	set("exits", ([

		"south" : __DIR__"nanmen",
		"north" : __DIR__"nandajie1",
	]));
	set("objects", ([
		CLASS_D("baiyun") + "/ye" : 1,
	]));
 	setup();
	replace_program(ROOM);
}
