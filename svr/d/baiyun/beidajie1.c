// Room: /city/beidajie1.c

inherit ROOM;
void create()
{
	set("short", "北大街");
	set("long", @LONG
你走在一条繁忙的街道上，心里不由得起一丝感伤。
LONG
	);
	set("outdoors", "baiyun");

	set("exits", ([
		"north" : __DIR__"beidajie2",
		"south" : __DIR__"guangchang",
		"east" : __DIR__"jingji/enter",
	]));
	replace_program(ROOM);
}
