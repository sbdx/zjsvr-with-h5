// Room: /city/ximenroad.c
inherit ROOM;
void create()
{
    	set("short", "树林");
	set("long", @LONG
传出阵阵阴森的感觉，细细品味又使人宁静，十分怪异。
LONG );
	set("outdoors", "baiyun");
	set("exits", ([
		"east"  : __DIR__"ximen",
		"south" : __DIR__"chuixue",
	]));

	setup();
}

int valid_leave(object me, string dir)
{
 
	if (dir == "south") me->set_temp("view_leitai",1);
	return ::valid_leave(me, dir);
}
