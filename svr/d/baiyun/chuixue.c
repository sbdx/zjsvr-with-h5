// Room: /city/nandajie2.c
// YZC 1995/12/04 
// CLEANSWORD 1996/2/2

inherit ROOM;

void create()
{
	set("short", "吹雪居");
	set("long", @LONG
这里便是阴森的源头。原来有人在练剑。
剑法无比的冷。孤独,仔细一看原来是个剑影,却不知主人如今何在!
LONG );
	set("outdoors", "baiyun");
	set("exits", ([
		"north" : __DIR__"ximenroad",
	]));
	set("objects", ([
		CLASS_D("baiyun") + "/ximen" : 1,
	]));
 	setup();
	replace_program(ROOM);
}
