// Room: /city/dongdajie2.c
// YZC 1995/12/04 

inherit ROOM;

void create()
{
	set("short", "东大街");
	set("long", @LONG
你走在东大街上，踩着坚实的青石板地面。东边是东城门，
LONG );
	set("outdoors", "baiyun");
	set("exits", ([
		"east"  : __DIR__"dongmen",
		"west"  : __DIR__"dongdajie1",
	]));

	setup();
	replace_program(ROOM);
}

