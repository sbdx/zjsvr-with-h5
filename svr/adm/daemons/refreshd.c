//refresh.c
#include <localtime.h>
inherit F_DBASE;
string *tlist = ({
	"/clone/weapon/green_stick",
	"d/shaolin/obj/fumo-zhang",
	"/d/shaolin/obj/jingang-zhao",
	"clone/weapon/xtbishou",
	"clone/weapon/jsbaojia",
	"/clone/book/wuji1",
	"/clone/book/wuji2",
	"/clone/book/wuji3",
	"/clone/book/wuji4",
	"/clone/book/qiankun_book",
	"/clone/book/six_book",
	"/clone/book/yijinjing",
	"/clone/book/lbook4",
	"/clone/book/jiuyin1",
	"/d/tulong/obj/zhenjing",
	"/clone/book/ling1",
	"/clone/book/ling2",
	"/clone/book/ling3",
	"/d/tulong/obj/tulongdao",
	"/d/tulong/obj/yitianjian",
	"/clone/weapon/zhenwu",
});
void create()
{
	seteuid(ROOT_UID);
	set("channel_id", "刷新精灵");
	CHANNEL_D->do_channel( this_object(), "sys", "刷新精灵已经启动。");
	set_heart_beat(14400);//60*60*2*4每8小时吧
}
void heart_beat(){
      
      object *list,ob;
      int i,k;
      for (i = 0; i < sizeof(tlist); i++)
     {
      list = children(tlist[i]);
		if(!list||!sizeof(list)) continue;
		for(k=0;k<sizeof(list);k++)
		{
			if((ob=environment(list[k]))&&playerp(ob))
			{
				tell_object(ob,HIR"系统特殊物品刷新，你身上的"NOR+list[k]->name()+HIR"消失了！\n"NOR);
				destruct(list[k]);
			}
		}
	}
  
  CHANNEL_D->do_channel( this_object(), "sys", "唯一物品刷新完毕。");
 }
