//   节日活动管理


///d/festival/x/x.c   修改节日日期

//2018.10.15 更改 mycmds   增加 /d/festival  目录    help目录下增加festival文件    global.h增加新预编译FESTIVAL_D   增加/adm/daemons/festivald文件.   /cmds/wiz/pay.c 更新节日双倍. 
//equipmentd.c boos击杀文件



#include <ansi.h>
#include <localtime.h>
inherit F_DBASE;
inherit F_CLEAN_UP;

#define FILE "/d/festival/"

int get_festival();
void festival_start(string *arg);
void festival_off();
int check_day();
void rebot_after();
void add_evet(int a);
int remaining_time();
string query_file();

void create()
{
	seteuid(getuid());
	set("channel_id","活动精灵");
	CHANNEL_D->do_channel( this_object(), "sys", "节日活动系统已经开启。");
	
	set_heart_beat(3);
	
//	set("channel_id","活动精灵");
//	set("festival",({10,14}));
}


void heart_beat()
{
	rebot_after();
	//check_day();
	//return ;
	
}

string query_file()
{
	string file;
	if(!this_object()->query("festival"))
	{
		return "";
	}
	file = FILE;
	file += this_object()->query("festival")[0];
	return file;
	
}




int get_festival()
{
	if(!(this_object()->query("festival")))
	{
		return 0;
	}
	return 1;
	
}

void festival_start(string *arg) //({"chongyang","重阳"})
{
	if(!arg) return ;
		
	this_object()->set("festival",arg);
	return ;
}

void festival_off()
{
	this_object()->delete("festival");
	return ;
}


void add_evet(int a)
{
	TIME_D->add_event((:festival_off:),a);//60*60*23+60*57*60); //23小时57分钟
	return ;
}


int remaining_time()
{
	string *str,*st,strin;
	int hour,min,sec,sum = 0;
	strin = uti_common_date(time());
	str = explode(strin,"日");
	st = explode( str[1] , ":" );
	hour = 23 - to_int(st[0]);
	min  = 59 - to_int(st[1]);
	sec  = 59 - to_int(st[2]);
    
	sum = hour*60*60 + min*60 + sec ;
	 
	return sum;
	
}


void rebot_after()
{
	if(this_object()->qury("check_day") != localtime(time())[LT_MDAY])
	{
	  if(check_day())
	 {
		this_object()->set("check_day", localtime(time())[LT_MDAY] );
		return ;
	 }
	 else
	 {
		 FESTIVAL_D->festival_off();
		 this_object()->set("check_day", localtime(time())[LT_MDAY] );
	 }
	  
	}
	return ;
	
}






int check_day()
{
	string *dir,file;
	int *days,mon,day;
	dir = get_dir("/d/festival/");
	
	if(!dir) return 0;
	for(int a=0;a<sizeof(dir);a++)
	{
		file = FILE;
		mon = localtime(time())[LT_MON]+1; // 值为0-11  实际应该+1
		day = localtime(time())[LT_MDAY];
		file += dir[a];

		if(file_size(file+"/"+dir[a]+".c") <0) //文件为空.
		{
		
			return 0;
		}
		file = file+ "/" +dir[a]+".c";
		days = ({mon,day});
 	
		if(file->check_days(days))
		{log_file("festival","进入\n");
			festival_start(   ({  file->query_festival_id()[0]  ,  file->query_festival_id()[1]  })     );
			
			add_evet(remaining_time()); //23小时57分钟
			return 1;
		}
		
		
	}
	festival_off();
	
	return 0;
}