//称号

//#pragma optinmize
//#pragma save_binary

#include <ansi.h>

//可加成基本属性值
mapping titles=([
"暗夜鬼魅":(["obtain":"\n辟邪剑法:500级","con":1,"dex":1,"int":1,"str":1,"max_qi":200]),
"北冥潇":(["obtain":"\n凌波微步:500级\n北冥神功:500级\n六阳掌:500级","con":1,"dex":1,"int":1,"str":1,"max_qi":200]),
"碧海潮生":(["obtain":"\n玉箫剑法:500级\n碧波神功:500级\n碧海潮生曲：500级","con":1,"dex":1,"int":1,"str":1,"max_qi":200]),
"毒仙":(["obtain":"\n冰蚕寒功:500级","con":1,"dex":1,"int":1,"str":1,"max_qi":200]),
"富可敌国":(["obtain":"\n元宝:99999","con":2,"dex":2,"int":2,"str":2,"max_qi":200]),
"剑魔":(["obtain":"\n独孤九剑:500级","con":1,"dex":1,"int":1,"str":1,"max_qi":200]),
"龙象之力":(["obtain":"\n龙象般若功:500级","con":1,"dex":1,"int":1,"str":1,"max_qi":200]),
"南慕容":(["obtain":"\n斗转星移:500级","con":1,"dex":1,"int":1,"str":1,"max_qi":200]),
//"奢华贵族":(["obtain":"\nvip:5级","con":2,"dex":2,"int":2,"str":2,"max_qi":200]),
"武者":(["obtain":"\n基本内功:200级","con":1,"dex":1,"int":1,"str":1,"max_qi":200]),
"太极":(["obtain":"\n太极神功:500级\n太极剑法:500级\n太极拳:500级","con":1,"dex":1,"int":1,"str":1,"max_qi":200]),
"侠客行":(["obtain":"\n太玄功:500级","con":1,"dex":1,"int":1,"str":1,"max_qi":200]),
"雪山飞狐":(["obtain":"\n冷月神功:500级\n胡家刀法:500级","con":1,"dex":1,"int":1,"str":1,"max_qi":200]),
"阳圣":(["obtain":"\n乾坤大挪移:500级\n九阳神功:500级","con":1,"dex":1,"int":1,"str":1,"max_qi":200]),
]);

//转换为中文名称
mapping chinesename=([
"obtain":"成就要求",
"max_neili":"内力上限",
"str":"后天臂力",
"int":"后天悟性",
"dex":"后天身法",
"con":"后天根骨",
"max_qi":"气血上线",
]);

//需要达标的属性值
mapping need_attribute=([
"富可敌国":(["yuanbao":99999]),
]);

//需要达标的技能
mapping need_skill=([
"暗夜鬼魅":(["pixie-jian":500]),
"北冥潇":(["beiming-shengong":500,"liuyang-zhang":500,"lingbo-weibu":500]),
"碧海潮生":(["bibo-shengong":500,"yuxiao-jian":500,"bihai-chaosheng":500]),
"武者":(["force":200]),
"毒仙":(["freezig-force":500]),
"剑魔":(["lonely-sword":500]),
"龙象之力":(["longxiang":500]),
"南慕容":(["douzhuan-xingyi":500]),
"侠客行":(["taixuan-gong":500]),
"雪山飞狐" : (["lengyue-shengong":500,"huajia-daofa":500]),
"太极":(["taiji-shengong":500,"taiji-jian":500,"taiji-quan":500]),
"阳圣":(["qiankun-danuoyi":500,"jiuyang-shengong":500]),
]);

void create(){seteuid(getuid());}

int use_title(object me,string title)
{
mixed xg=titles[title];
string *att,at;
att=keys(xg);
me->delete("tltlea");
foreach(at in att)
{
if(intp(xg[at]))
me->set("titlea/"+at,xg[at]);
}
me->set("chenghao",title);
tell_object(me,"你佩戴了成就:"HIC+title+NOR+"\n");
return 1;
}

string query_title(object me,string title)
{
string msg,msgg;
mixed xg=titles[title];
if(xg)
{
msg=chinesename["obtain"]+":"+xg["obtain"]+"\n";
msg=title+"佩戴成就奖励:"ZJBR"";
if(xg["sm"])
	msg+=chinesename["sm"]+":\n"+xg["sm"]+"\n";
foreach (msgg in keys(xg))
if(intp(xg[msgg]))
msg+=chinesename[msgg]+":"+xg[msgg]+"\n";
msg+=chinesename["obtain"]+":"+xg["obtain"];
}
else
msg="该成就无任何加成\n";
return msg;
}

mapping alltitles()
{
return titles;
}
//购买称号处理
int buy_title(object me,string title)
{
string att,*allatt;
mapping attribute;
//先计算属性是否达标
if(me->query("titles/"+title))
{
tell_object(me,"你已经完成这个成就了\n");
return 1;
}

if (need_attribute[title])
{
allatt=keys(need_attribute[title]);
attribute=need_attribute[title];
foreach(att in allatt)
if(me->query(att)<attribute[att])
{
tell_object(me,"你为达到获得成就的要求\n");
return 1;
}
}
//再计算技能是否达标
if(need_skill[title])
{
allatt=keys(need_skill[title]);
attribute=need_skill[title];
foreach(att in allatt)
if(me->query_skill(att,1)<attribute[att])
{
tell_object(me,"你的"+to_chinese(att)+"低于"+(attribute[att])+"级。\n");
return 1;
}
}
//最后计算声望是否足够
attribute=titles[title];
if(me->query("reputation")<attribute["reputation"])
{
tell_object(me,"你的声望不够。\n");
return 1;
}
me->set("titles/"+title,1);
tell_object(me,"恭喜你获得成就"+HIC+title+"\n"NOR);
return 1;
}
