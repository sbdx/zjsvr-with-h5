// Written By Lonely@Nitan3

#include <ansi.h>

//#pragma optimize
//#pragma save_binary
#define GEM_DIR "/clone/enchase/gem/"
string *gem_name = ({
        "宝石碎片",
        "宝石",
        "稀世宝石",
        "帝之宝石",
        "圣之宝石",
        "魔之宝石",
        "神之宝石",
});

string *skull_name = ({
        "骷髅头碎片",
        "骷髅头",
        "稀世骷髅头",
        "帝之骷髅头",
        "圣之骷髅头",
        "魔之骷髅头",
        "神之骷髅头",
});
//鉴定
mixed identify_ob(object me, object ob)
{
        int     i, level;
        string  filename, color, id;
        string  *types, base_file;
        mixed   obj;
        mapping data, temp, dbase;

        data = ([ ]);
        temp = ([ ]);
        dbase = ([ ]);

        filename = base_name(ob);
        filename += ".c";

        if (filename == GEM_DIR+"skull.c"){//初级骷髅 最高机率4级
                if (random(100) == 0) level = 4;
                else if (random(50) == 0) level = 3;
                else if (random(10) == 0) level = 2;
                else level = 1;

                base_file = "skull";
                id = base_file + level;
        } else if (sscanf(filename, GEM_DIR+"skull%d.c", level)){//带等级骷髅
                base_file = "skull";
                id = base_file + level;
        } else if (sscanf(filename, GEM_DIR+"%sgem.c", color)){//初级宝石
                if (random(100) == 0) level = 4;
                else if (random(50) == 0) level = 3;
                else if (random(10) == 0) level = 2;
                else level = 1;

                base_file = "gem";
                id = color + " " + base_file + level;
        } else if (sscanf(filename, GEM_DIR+"%sgem%d.c", color, level) == 2){//带等级宝石
                base_file = "gem";
                id = color + " " + base_file + level;
        } else{
        	 return 0;
        }
               
//暂时不开放兵器属性 因为这个LIB 空手武功不能打造武器！
//        dbase = ([ ]);
//        types = EQUIPMENT_D->apply_stats("weapon", level / 2);
//        for (i = 0; i < level/2; i++)
//                dbase[types[i]] = EQUIPMENT_D->query_stats_value(types[i], level);
//        temp["weapon_prop"] = dbase;

        dbase = ([ ]);
        types = EQUIPMENT_D->apply_stats("armor", level / 2);
        for (i = 0; i < level/2; i++)
                dbase[types[i]] = EQUIPMENT_D->query_stats_value(types[i], level);
        temp["armor_prop"] = dbase;

        temp["level"] = level;
        temp["SN"] = 1 + random(9);
        temp["consistence"] = 80 + random(21);

        if (! stringp(color) || ! color)
                data["name"] = HIB + skull_name[level-1] + NOR;
        else
                data["name"] = convert_color(upper_case(color)) + gem_name[level-1] + NOR;

        data["enchase"] = temp;
        
        /*废除魔力属性
        if (level == 7)
        {//只有七级宝石才会有魔力属性，才能镶嵌
                temp = ([ "type" : "magic", "power" : 15 + random(16) ]);
                data["magic"] = temp;
                data["can_be_enchased"] = 1;
        }*/
        
        
        if (level == 7)
        {	//只有七级宝石才会有魔力属性，才能镶嵌
        	//暂时不区分颜色
        	
        	data["can_be_enchased"] = 1;
        }
        
        
        

        obj = TEMPLATE_D->create_object("/template/gem/" + base_file, id, data, 1);

        return obj;
}
// 无上神品->上古神品->中古神品->远古神品->太古神品
// 太始、太初、太玄 太虚、洪荒
// 冥古宙、太古宙、元古宙和显生宙
// 其中元古宙又划分为古元古代、中元古代和新元古代；
// 显生宙划分古生代、中生代和新生代。
varargs int identify_ultimate_ob(object item, int close,int full)
{
        mapping data, prop;
        mixed *inset, *apply;
        int i, n, s;
        int r_effect, d_effect, p_effect,
			max_neili, max_jingli,max_qi,max_jing,max_potential,max_experience;

        r_effect = 0;
        d_effect = 0;
        p_effect = 0;

//
        if (item->query("skill_type") ||  (item->query("armor_type") && item->query("armor_type") == "hands")){
        	write("目前只开放防具洗练!\n");
        	return 0;
        }


        s = (int)item->query("enchase/SN");
        if (s == 37 && ! item->query("ultimate/37"))
        {
                item->add("enchase/apply_prop/research_effect", 10);
                if (item->query("enchase/apply_prop/research_effect") > 40)
                        item->set("enchase/apply_prop/research_effect", 40);
                item->add("enchase/apply_prop/practice_effect", 10);
                if (item->query("enchase/apply_prop/practice_effect") > 40)
                        item->set("enchase/apply_prop/practice_effect", 40);
                item->add("enchase/apply_prop/derive_effect", 10);
                if (item->query("enchase/apply_prop/derive_effect") > 40)
                        item->set("enchase/apply_prop/derive_effect", 40);
               
                item->add("enchase/SN", -(random(9) + 1));
                item->set("ultimate/37", 1);
                item->save();
                return 1;
        } else  if (s == 39 && ! item->query("ultimate/39"))
        {
                item->add("enchase/apply_prop/research_effect", 15);
                if (item->query("enchase/apply_prop/research_effect") > 40)
                        item->set("enchase/apply_prop/research_effect", 40);
                item->add("enchase/apply_prop/practice_effect", 15);
                if (item->query("enchase/apply_prop/practice_effect") > 40)
                        item->set("enchase/apply_prop/practice_effect", 40);
                item->add("enchase/apply_prop/derive_effect", 15);
                if (item->query("enchase/apply_prop/derive_effect") > 40)
                        item->set("enchase/apply_prop/derive_effect", 40);
                
                item->set("ultimate/39", 1);
                item->save();
                return 1;
        } else if (s == 69 && ! item->query("ultimate/69")  && random(50) < 10  /*&& item->query("armor_type") && item->query("armor_type") != "hands"*/)
        	
        {
                data = item->query("enchase/apply_prop");
                if (! mapp(data)) data = ([ ]);
                apply = keys(data);
                for(i=0; i<sizeof(apply); i++)
                        data[apply[i]] = data[apply[i]] * 3 / 2;

                inset = item->query("enchase/inset");
                if (! inset) inset = ({});
                n = sizeof(inset);
                for (i = 0; i < n; i++)
                {
                        if (mapp(prop = inset[i]["enchase_prop"]) && sizeof(prop))
                        {
                                if (! undefinedp(prop["research_effect"]))
                                        r_effect += prop["research_effect"];
                                if (! undefinedp(prop["derive_effect"]))
                                        d_effect += prop["derive_effect"];
                                if (! undefinedp(prop["practice_effect"]))
                                        p_effect += prop["practice_effect"];
                                if (! undefinedp(prop["max_jingli"]))
                                		max_jingli += prop["max_jingli"];
                                if (! undefinedp(prop["max_potential"]))
                                	max_potential += prop["max_potential"];
                                if (! undefinedp(prop["max_experience"]))
                                	max_experience += prop["max_experience"];
                        }
                }
                data["research_effect"] = r_effect * 2 / 2;
                data["derive_effect"] 	= d_effect * 2 / 2;
                data["practice_effect"] = p_effect * 2 / 2;
                //max_potential,max_experience;
                //提升精力
                data["max_jingli"]      = max_jingli + EQUIPMENT_D->query_stats_value("max_jingli", 1 + random(2));
                data["max_potential"]   = max_potential + EQUIPMENT_D->query_stats_value("max_potential", 1 + random(2));
                data["max_experience"]  = max_experience + EQUIPMENT_D->query_stats_value("max_experience", 1 + random(2));




                item->set("enchase/apply_prop", data);
               // item->add("enchase/flute", 1);//会有问题无法控制是兵器还是防具
                item->set("ultimate/69", 1);
                item->set("ultimate/ob", 1);
                item->save();

                if (! close)
                CHANNEL_D->do_channel(find_object(ITEM_D), "rumor",
                              "听说中古神品" + item->name() + HIM +
                              "来到了人间。");


                log_file("static/ultimate", sprintf("%s Have 69 SN. Wash %d Times. %s\n",
                          base_name(item), item->query("enchase/wash"), ctime(time())));
                return 1;
        } else if (s == 87 && ! item->query("ultimate/87") && random(500) < 10 )//by fang add random
        {
//                data = item->query("enchase/apply_prop");
//                if (! mapp(data)) data = ([ ]);
//                apply = keys(data);
//                if (item->query("skill_type") ||  (item->query("armor_type") && item->query("armor_type") == "hands"))
//                {//兵器类

//                        item->set("ultimate/87", 1);
//
////                        //兵器九孔
////                        if ( item->query("enchase/flute") < 9 ){
////                        	item->add("enchase/flute", 1);
////                        }
//
//
//
//
//                        //防具七孔
////                        if ( item->query("enchase/flute") < 7 ){
////                        	item->add("enchase/flute", 1);
////                        }
//
//
//                        if (! close)
//                        CHANNEL_D->do_channel(find_object(ITEM_D), "rumor",
//                                "听说远古神品" + item->name() + HIM +
//                                "来到了人间。");
//                }
//
//                item->set("ultimate/ob", 1);
//                item->save();
//                log_file("static/ultimate", sprintf("%s Have 87 SN. Wash %d Times. %s\n",
//                          base_name(item), item->query("enchase/wash"), ctime(time())));






            data = item->query("enchase/apply_prop");
            if (! mapp(data)) data = ([ ]);
            apply = keys(data);
            for(i=0; i<sizeof(apply); i++)
                    data[apply[i]] = data[apply[i]] * 3 / 2;

            inset = item->query("enchase/inset");
            if (! inset) inset = ({});
            n = sizeof(inset);
            for (i = 0; i < n; i++)
            {
                    if (mapp(prop = inset[i]["enchase_prop"]) && sizeof(prop))
                    {
                            if (! undefinedp(prop["research_effect"]))
                                    r_effect += prop["research_effect"];
                            if (! undefinedp(prop["derive_effect"]))
                                    d_effect += prop["derive_effect"];
                            if (! undefinedp(prop["practice_effect"]))
                                    p_effect += prop["practice_effect"];
                            if (! undefinedp(prop["max_jingli"]))
                            		max_jingli += prop["max_jingli"];
                            if (! undefinedp(prop["max_potential"]))
                            	max_potential += prop["max_potential"];
                            if (! undefinedp(prop["max_experience"]))
                            	max_experience += prop["max_experience"];
                    }
            }
            data["research_effect"] = r_effect * 3 / 2;
            data["derive_effect"] 	= d_effect * 3 / 2;
            data["practice_effect"] = p_effect * 3 / 2;

            //提升精力
            data["max_jingli"]      = max_jingli + EQUIPMENT_D->query_stats_value("max_jingli", 2 + random(2));
            data["max_potential"]   = max_potential + EQUIPMENT_D->query_stats_value("max_potential", 2 + random(2));
            data["max_experience"]  = max_experience + EQUIPMENT_D->query_stats_value("max_experience", 2 + random(2));




            item->set("enchase/apply_prop", data);
           // item->add("enchase/flute", 1);//会有问题无法控制是兵器还是防具
            item->set("ultimate/87", 1);
            item->set("ultimate/ob", 1);
            item->save();

            if (! close)
            CHANNEL_D->do_channel(find_object(ITEM_D), "rumor",
                          "听说远古神品" + item->name() + HIM +
                          "来到了人间。");


            log_file("static/ultimate", sprintf("%s Have 87 SN. Wash %d Times. %s\n",
                      base_name(item), item->query("enchase/wash"), ctime(time())));





                return 1;
        } else if (s == 105 && ! item->query("ultimate/105") && random(1000) < 10 )//by fang add random
        {//开始加内力
                data = item->query("enchase/apply_prop");
                if (! mapp(data)) data = ([ ]);
                apply = keys(data);
                for(i=0; i<sizeof(apply); i++)
                        data[apply[i]] *= 2;

                inset = item->query("enchase/inset");
                if (! inset) inset = ({});
                n = sizeof(inset);
                for (i = 0; i < n; i++)
                {
                        if (mapp(prop = inset[i]["enchase_prop"]) && sizeof(prop))
                        {
                                if (! undefinedp(prop["research_effect"]))
                                        r_effect += prop["research_effect"];
                                if (! undefinedp(prop["derive_effect"]))
                                        d_effect += prop["derive_effect"];
                                if (! undefinedp(prop["practice_effect"]))
                                        p_effect += prop["practice_effect"];
                                if (! undefinedp(prop["max_jingli"]))
                                		max_jingli += prop["max_jingli"];
                                if (! undefinedp(prop["max_neili"]))
                                		max_neili += prop["max_neili"];
                                if (! undefinedp(prop["max_potential"]))
                                	max_potential += prop["max_potential"];
                                if (! undefinedp(prop["max_experience"]))
                                	max_experience += prop["max_experience"];
                        }
                }

                 data["research_effect"] 	= r_effect * 4 / 2;
                 data["derive_effect"] 		= d_effect * 4 / 2;
                 data["practice_effect"] 	= p_effect * 4 / 2;
                 //这几个属性不能乱加
                 //max_neili, max_jingli,max_qi,max_jing,max_potential,max_experience;

                 data["max_jingli"]      = max_jingli + EQUIPMENT_D->query_stats_value("max_jingli", 3 + random(2));
                 data["max_neili"]       = max_neili + EQUIPMENT_D->query_stats_value("max_neili", 3 + random(2));
                 data["max_potential"]   = max_potential + EQUIPMENT_D->query_stats_value("max_potential",3 + random(2));
                 data["max_experience"]  = max_experience + EQUIPMENT_D->query_stats_value("max_experience", 3 + random(2));

                item->set("enchase/apply_prop", data);
                item->set("ultimate/105", 1);
                item->set("ultimate/ob", 1);
                item->save();
                if (! close){
                    CHANNEL_D->do_channel(find_object(ITEM_D), "rumor","听说上古神品" + item->name() + HIM +"来到了人间。");
                }


                log_file("static/ultimate", sprintf("%s Have 105 SN. Wash %d Times. %s\n",
                          base_name(item), item->query("enchase/wash"), ctime(time())));
                return 1;
        } else if (s == 121 && ! item->query("ultimate/121") && random(200000) < 10)//by fang add random  增加难度
        {		
          
        		//老十兵特效
          
        		
                data = item->query("enchase/apply_prop");
                if (! mapp(data)) data = ([ ]);
                apply = keys(data);
                for(i=0; i<sizeof(apply); i++)
                        data[apply[i]] *= 3;

                inset = item->query("enchase/inset");
                if (! inset) inset = ({});
                n = sizeof(inset);
                for (i = 0; i < n; i++)
                {
                        if (mapp(prop = inset[i]["enchase_prop"]) && sizeof(prop))
                        {
                                if (! undefinedp(prop["research_effect"]))
                                        r_effect += prop["research_effect"];
                                if (! undefinedp(prop["derive_effect"]))
                                        d_effect += prop["derive_effect"];
                                if (! undefinedp(prop["practice_effect"]))
                                        p_effect += prop["practice_effect"];
                                if (! undefinedp(prop["max_jingli"]))
										max_jingli += prop["max_jingli"];
								if (! undefinedp(prop["max_neili"]))
										max_neili += prop["max_neili"];
                                if (! undefinedp(prop["max_potential"]))
                                	max_potential += prop["max_potential"];
                                if (! undefinedp(prop["max_experience"]))
                                	max_experience += prop["max_experience"];
                        }
                }

                data["research_effect"] 	= r_effect * 8 / 2;
                data["derive_effect"] 		= d_effect * 8 / 2;
                data["practice_effect"] 	= p_effect * 8 / 2;
                //这几个属性不能乱加
                //max_neili, max_jingli,max_qi,max_jing,max_potential,max_experience;


                data["max_jingli"]      = max_jingli + EQUIPMENT_D->query_stats_value("max_jingli", 5 + random(2));
                data["max_neili"]       = max_neili + EQUIPMENT_D->query_stats_value("max_neili", 	5 + random(2));
                data["max_potential"]   = max_potential + EQUIPMENT_D->query_stats_value("max_potential",5 + random(2));
                data["max_experience"]  = max_experience + EQUIPMENT_D->query_stats_value("max_experience", 5 + random(2));



                item->set("enchase/apply_prop", data);
                item->set("ultimate/121", 1);
                item->set("ultimate/ob", 1);
                
                //激活 老十兵系统属性
//                ownerid = item->item_owner();
//                item->set("combat/MKS", 2100);
//                item->set("owner/" + ownerid, 21000000);
//                item->set("magic/blood", 21000);
//
//                item->set("magic/power", 90 + random(11));
//                item->set("magic/type", mg_type[random(sizeof(mg_type)-1)] );
                //激活 老十兵系统属性 end
                
                item->save();
                
                
                
                //内置数据库保存
                DBASE_D->save();
                if (! close)
                CHANNEL_D->do_channel(find_object(ITEM_D), "rumor",
                              "听说太古神品" + item->name() + HIM +
                              "来到了人间。");

                log_file("static/ultimate", sprintf("%s Have 121 SN. Wash %d Times. %s\n",
                          base_name(item), item->query("enchase/wash"), ctime(time())));
                return 1;
        } else if ( full==212 || (s == 212 && ! item->query("ultimate/212") && random(10000000) < 10 ) )
        {		
        		string ownerid;
        		//老十兵特效
        		string *mg_type=({"magic","fire","cold","lighting"});
        		
                data = item->query("enchase/apply_prop");
                if (! mapp(data)) data = ([ ]);
                apply = keys(data);
                for(i=0; i<sizeof(apply); i++)
                        data[apply[i]] *= 5;

                inset = item->query("enchase/inset");
                if (! inset) inset = ({});
                n = sizeof(inset);
                for (i = 0; i < n; i++)
                {
                        if (mapp(prop = inset[i]["enchase_prop"]) && sizeof(prop))
                        {
                                if (! undefinedp(prop["research_effect"]))
                                        r_effect += prop["research_effect"];
                                if (! undefinedp(prop["derive_effect"]))
                                        d_effect += prop["derive_effect"];
                                if (! undefinedp(prop["practice_effect"]))
                                        p_effect += prop["practice_effect"];
                                if (! undefinedp(prop["max_jingli"]))
										max_jingli += prop["max_jingli"];
								if (! undefinedp(prop["max_neili"]))
										max_neili += prop["max_neili"];
                                if (! undefinedp(prop["max_potential"]))
                                	max_potential += prop["max_potential"];
                                if (! undefinedp(prop["max_experience"]))
                                	max_experience += prop["max_experience"];
                        }
                }



				data["research_effect"] 	= r_effect * 20 / 2;
				data["derive_effect"] 		= d_effect * 20 / 2;
				data["practice_effect"] 	= p_effect * 20 / 2;
				//这几个属性不能乱加
				//max_neili, max_jingli,max_qi,max_jing,max_potential,max_experience;
                data["max_jingli"]      = max_jingli + EQUIPMENT_D->query_stats_value("max_jingli", 5 + random(2)) * 10;
                data["max_neili"]       = max_neili + EQUIPMENT_D->query_stats_value("max_neili", 	5 + random(2)) * 10;
                data["max_potential"]   = max_potential + EQUIPMENT_D->query_stats_value("max_potential",5 + random(2));
                data["max_experience"]  = max_experience + EQUIPMENT_D->query_stats_value("max_experience", 5 + random(2));


                item->set("enchase/apply_prop", data);
                item->set("ultimate/212", 1);
                item->set("ultimate/ob", 1);
                
                if(full==212){
                	item->set("enchase/SN", 212);
                }
                
                //激活 老十兵系统属性
                ownerid = item->item_owner();
                item->set("combat/MKS", 2100);
                item->set("owner/" + ownerid, 21000000);
                item->set("magic/blood", 21000);
                
                item->set("magic/power", 90 + random(11));
                item->set("magic/type", mg_type[random(sizeof(mg_type)-1)] );
                //item->set("magic/tessera", tessera->name());
                
                item->save();
                
                
                
                //内置数据库保存
                DBASE_D->save();
                if (! close)
                CHANNEL_D->do_channel(find_object(ITEM_D), "rumor",
                              "听说太初神品" + item->name() + HIM +
                              "来到了人间。");

                log_file("static/ultimate", sprintf("%s Have 212 SN. Wash %d Times. %s\n",
                          base_name(item), item->query("enchase/wash"), ctime(time())));
                return 1;
        }else
                return 0;
}