#include <ansi.h>

inherit NPC;

string ask_lilian();
string ask_jieshu();
string ask_baotu();
string ask_pay();
string ask_yao();
int ask_gift();
string ask_newgift();
string ask_tap();
string ask_tap2();
string ask_buchang();
string ask_xinwu();
string ask_jieribox();
string ask_weihu();
string ask_gujin();

void create()
{
	set_name(HIW"信陵君"NOR, ({ "diyu shizhe", "shizhe" }));
	set("nickname", HBCYN"信陵仙人"NOR);
	set("long", "这位是武林传说信陵至尊，负责处理信陵仙境的事情。\n");
	set("gender", "男性");
	set("age", 30);
	set("per", 24);
	set("str", 40);
	set("int", 40);
	set("con", 40);
	set("dex", 40);
	set("qi",5000);
	set("jingli",6000);
	set("max_qi", 8000);
	set("max_jing", 5000);
	set("eff_jingli", 6000);
	set("jiali", 190);
	set("combat_exp", 9000000);
	set("shen", 25000);

	set("chat_chance_combat", 60);
	set("chat_msg_combat", ({
		(: exert_function, "taiji" :),
		(: perform_action, "sword.chan" :),
		(: perform_action, "cuff.zhen" :),
		(: perform_action, "cuff.zhan" :),
		(: perform_action, "cuff.ji" :)
	}));

	set("inquiry", ([
		"历练":(: ask_lilian:),
		"结束" : (: ask_jieshu:),
		"贡献" : (: ask_pay:),
		"护心丹" : (: ask_yao:),
		"藏宝图" : (: ask_baotu:),
		"开荒新手礼包" : (: ask_newgift:),
		"渠道上架礼包" : (: ask_tap:),
		"好游上架礼包" : (: ask_tap2:),
		"维护礼包" : (: ask_weihu:),
		"古今礼包" : (:ask_gujin:),
		"节日礼包" : (: ask_jieribox:),
		"宕机补偿" : (: ask_buchang:),
		
	]));
	setup();
}
string ask_tap()
{
	object ob = this_player();
	object gift;
 
	
///	if (!ob->query("tap_pinglun"))
	//:	return ("内含一本孟子，两颗九转。评论后找巫师领取。。");
			
	if (ob->query("newgift_give_box100"))
		return ("你已经领取过开荒大礼包了，不能再次领取，感谢您的支持。");
	
	command("nod "+ob->query("id"));
	command("say 我这就发给你。");
	gift = new("/clone/tap_box");
    gift->move(ob);	
	tell_object(ob,HIM"你获得了一个开荒宝盒。"NOR"\n");  
	
	ob->set("newgift_give_box100", 1);
	return "祝你游戏愉快！";
}

string ask_gujin() {//函数
	object ob = this_player();//定义一个名为ob的对象变量，他的值是调用这个函数的玩家对象
 object gift;//定义一个gift对象=_=就是礼物
	if (ob->query("weihu_7"))//查看玩家是不是领取过了
		return ("你已经领取过从古至今礼包了，不能再次领取了，祝你游戏快乐哦。");
	//if语句内容只有一行是可以省略中括号
//给玩家一点提示
	command("nod " + ob->query("id"));//发送命令
	command("say 我这就发给你。");//发送命令
	gift = new("/clone/vip/vip2/putao1.c");//复制一个指定路径的对象，把他负值给gift
	gift->set_amount(28);//设定这个对象的数量
	gift->move(ob);//把这个对象移动到玩家对象里面(背包)
	tell_object(ob, HIM"你获得了28个大葡萄。"NOR"\n");//告诉玩家你得到了东西
	//=_=下冲你自己写吧，开卷考试
	//gif = new("/clone/vip/vip2/dan_chongmai1");//复制一个指定路径的对象，把他负值给gift
	//gif->set_amount(88);//设定这个对象的数量
	//gif->move(ob);//把这个对象移动到玩家对象里面(背包)
	//tell_object(ob, HIM"你获得了88个下冲。"NOR"\n");//告诉玩家你得到了东西
	ob->set("weihu_7", 1);
	return "信陵君在此谢过您一直以来的陪伴！！";
	//给玩家设定一个参数表示他领取过了
}

string ask_jieribox() {//函数
	object ob = this_player();//定义一个名为ob的对象变量，他的值是调用这个函数的玩家对象
	object gift, gif;//定义一个gift对象=_=就是礼物
	if (ob->query("gift_box6"))//查看玩家是不是领取过了
		return ("你已经领取过儿童节礼包了，不能再次领取了，祝你儿童节快乐哦，永远年轻。");
	//if语句内容只有一行是可以省略中括号
//给玩家一点提示
	command("nod " + ob->query("id"));//发送命令
	command("say 我这就发给你。");//发送命令
	gift = new("/clone/vip/vip2/dan_jiuzhuan");//复制一个指定路径的对象，把他负值给gift
	gift->set_amount(3);//设定这个对象的数量
	gift->move(ob);//把这个对象移动到玩家对象里面(背包)
	tell_object(ob, HIM"你获得了3个九转金丹。"NOR"\n");//告诉玩家你得到了东西
	//=_=下冲你自己写吧，开卷考试
	gif = new("/clone/vip/vip2/butian.c");//复制一个指定路径的对象，把他负值给gift
	gif->set_amount(1);//设定这个对象的数量
	gif->move(ob);//把这个对象移动到玩家对象里面(背包)
	tell_object(ob, HIM"你获得了1个补天石。"NOR"\n");//告诉玩家你得到了东西
	ob->set("gift_box6", 1);
	return "醉红尘工作室祝您五二零快乐！！";
	//给玩家设定一个参数表示他领取过了
}

string ask_tap2()
{
	object ob = this_player();
	object gift;
 
	
///	if (!ob->query("tap_pinglun"))
	//:	return ("内含一本孟子，两颗九转。评论后找巫师领取。。");
			
	if (ob->query("newgift_give_box7685664"))
		return ("你已经领取过好游快爆上架礼包了，不能再次领取，感谢您的支持。");
	
	command("nod "+ob->query("id"));
	command("say 我这就发给你。");
	gift = new("/clone/vip/vip2/tianxiang");
	gift->set_amount(10);
    gift->move(ob);	
	tell_object(ob,HIM"你获得了十枚仙药天香玉露。"NOR"\n");  

	gift = new("/clone/vip/vip2/putao1");
	gift->set_amount(5);
	gift->move(ob);
	tell_object(ob, HIM"你获得了五串神奇葡萄。"NOR"\n");
	
	ob->set("newgift_give_box7685664", 1);
	return "祝你游戏愉快！";
}
string ask_xinwu()
{
	object ob;
	mapping myfam;
	object nang;

	ob = this_player();
	myfam = (mapping)ob->query("family");
	
	if (ob->query("combat_exp") < 1500000)
		return "凭你的能力根本不配领取瑾墨青花。\n";

	if ( present("jinmo qinghua", ob))
		return "你身上不是有瑾墨青花吗？别贪得无厌！\n";

	nang=new("/clone/weapon/yupei");
	nang->set("owner_id", ob->query("id"));
	nang->move(ob);
	ob->set("yupei/qinghua", 1);
	write("你得到了一块"+nang->name(1)+"\n");
	return "这件东西我就送给你了，如果掉了还可以找我领取。";
}

string ask_buchang()
{
	object ob = this_player();
	object gift;
 
 

	/*if (!wizardp(ob))
		return ("测试完成才可以领取。");*/
		/*today = localtime(time());
		if ((today[4]+1)==5 && today[3] >= 30 || (today[4]+1)==6 && today[3] <= 2) {
		/**/
	if (ob->query("dangji_2"))
		return ("你已经领取过宕机补偿礼包了，不能再次领取，感谢您的支持和理解。");

	command("nod " + ob->query("id"));
	command("say 我这就发给你。");
	gift = new("/clone/vip/vip2/putao1.c");
	gift->set_amount(20);
	tell_object(ob, HIM"你获得了" + gift->short() + "。"NOR"\n");
	log_file("new_gift/weihu", sprintf("%s：%s(%s)领取了%s。\n",
		ctime(time()), ob->query("name"), ob->query("id"), gift->short()));
	ob->set("dangji_2", 1);
	gift->move(ob);
	return "祝你游戏愉快！";
}

string ask_weihu()
{
	object ob = this_player();
	object gift;
 
 

	/*if (!wizardp(ob))
		return ("测试完成才可以领取。");*/
		/*today = localtime(time());
		if ((today[4]+1)==5 && today[3] >= 30 || (today[4]+1)==6 && today[3] <= 2) {
		/**/
	if (ob->query("weihu_38"))
		return ("你已经领取过维护礼包了，不能再次领取，感谢您的支持和理解。");

	command("nod " + ob->query("id"));
	command("say 我这就发给你。");
	gift = new("/clone/vip/vip2/putao1.c");
	gift->set_amount(10);
	tell_object(ob, HIM"你获得了" + gift->short() + "。"NOR"\n");
	log_file("new_gift/weihu", sprintf("%s：%s(%s)领取了%s。\n",
		ctime(time()), ob->query("name"), ob->query("id"), gift->short()));
	ob->set("weihu_38", 1);
	gift->move(ob);
	return "祝你游戏愉快！";

}

string ask_newgift()
{
	object ob = this_player();
	object gift;
 

	//if (!wizardp(ob))
	//	return ("正式开站才可以领取。");

	if (ob->query("newgift_give_box3"))
		return ("你已经领取过开荒新手礼包了，不能再次领取，感谢您的支持。");

	command("nod " + ob->query("id"));
	command("say 我这就发给你。");
	gift = new("/clone/gong");
	gift->move(ob);
	gift = new("/clone/jian");
	gift->set_amount(99);
	gift->move(ob);
	tell_object(ob, HIM"你获得了一把弓，99根箭。"NOR"\n");
	ob->set("zjvip/times", 84600 * 15);
	ob->set("combat_exp", 200000);
	ob->set("potential", 500000);
	ob->set("weiwang", 5000);
	ob->set("score", 3000);
	ob->add("yuanbao_2", 1000);
	ob->set_skill("force", 100);
	ob->set_skill("dodge", 100);
	ob->set_skill("parry", 100);
	ob->set_skill("sword", 100);
	ob->set_skill("unarmed", 100);
	ob->set_skill("cuff", 100);
	ob->set_skill("blade", 100);
	ob->set_skill("zhanshenjue", 100);
	

	tell_object(ob, HIM"增加了20w经验，50w潜能，100元宝票，100级武功，30天会员，5000威望3000阅历。"NOR"\n");

	log_file("new_gift/kaihuang", sprintf("%s：%s(%s)领取了%s。\n",
		ctime(time()), ob->query("name"), ob->query("id"), gift->short()));
	ob->save();
	ob->set("newgift_give_box3", time());
	return "祝你游戏愉快！";
}

void init()
{
	object ob;
	::init();//继承上一级
	if (interactive(ob = this_player()) && !is_fighting()) {
		remove_call_out("greeting");
		call_out("greeting", 0, ob);
	}
}

void greeting(object ob)
{
	if( !ob || environment(ob) != environment() ) return;
	if( ob->query("zjvip/times") > 0 && !ob->query("lilian/start")) {
		command("hi "+ob->query("id"));
		command("whisper "+ob->query("id")+" "HIC"\n这位" + RANK_D->query_respect(ob) +"你是我们信陵仙境的成员，\n"+
			"你还没有在我这里"HIC""ZJURL("cmds:ask shizhe about 历练")+ZJSIZE(20)"登记"NOR""HIR"历练"HIC"，\n"+
			"赶紧开始你的"HIR"历练"HIC"，领取丰厚的家族奖励吧！"NOR"");
	}

}


int ask_gift()
{
	object ob = this_player();
	object gift;
	int num,money;

	if ((money=VIP_D->query_gift_payrec("春节/"+ob->query("id")))<100) {
		command("say 你目前剩余的节日充值累计为"+money+"，不够领取充值礼包！");
		return 1;
	}

	command("nod "+ob->query("id"));
	command("say 我这就发给你。");
	num = money/100;
	VIP_D->set_gift_payrec("春节/"+ob->query("id"),money-num*100);
	gift = new("/clone/gift/gift_newyear_pay");
	gift->set_amount(num);
	tell_object(ob,HIM"你获得了"+gift->short()+"。"NOR"\n");  
	log_file("buchang", sprintf("%s：%s(%s)领取了%s。\n",ctime(time()),ob->query("name"),ob->query("id"),gift->short()));

	gift->move(ob);
	return 1;
        
}


string ask_lilian()
{ 
	object me = this_object();
	object ob = this_player();

        if (ob->query("vip/all_pay")>0) {
            ob->add("zjvip/all_pay",ob->query("vip/all_pay"));
            ob->delete("vip/all_pay");
            }

        if (ob->query("zjvip/times") <1) return "对不起，我只处理信陵仙境的事情！\n";

        if (ob->query("lilian/start")) return "我已经记录了你的历练信息，放心去吧。\n";

	message_vision(HIG"$N翻开记事簿，对$n点了点头，说道：好吧，我这就为你记上。"NOR"\n",me,ob);

	ob->set("lilian/start",time());

	return "别忘了在六个时辰内找我结束历练。\n"; 
}

string ask_jieshu()
{
	  object me = this_object();
	  object ob = this_player();
        int time3, paid,exp,gold;

        time3=time()-(int)ob->query("lilian/start");

        if (ob->query("zjvip/times") <1) return "对不起，我只处理信陵仙境的事情！\n";

        if (!ob->query("lilian/start")) return "我这里没有你的历练记录。\n";

        paid=ob->query("zjvip/all_pay");

        tell_object(ob,HIC"你的累计历练时间是"+CHINESE_D->chinese_time(time3)+"。"NOR"\n");  

        if (time3>43200) time3=43200;

        tell_object(ob,HIC"你的有效历练时间是"+CHINESE_D->chinese_time(time3)+"。"NOR"\n");  
	  message_vision(HIY"$N结束了$n的本次历练！"NOR"\n",me,ob);
        ob->add("balance",time3*2);
        ob->add("combat_exp",time3/4);
        ob->add("potential",time3/28);
        ob->delete("lilian/start");
        tell_object(ob,"你被奖励了\n"
                       "白银："+time3*2/100+" 两，\n"
                       "经验："+time3/4+" 点，\n"
                       "潜能："+time3/28+" 点。"NOR"\n");  


        exp=time3*paid/1000;
        if (exp>time3) exp=time3;
        gold=time3*paid/1000;
        if (gold>time3) gold=time3;

        if (ob->query("id")=="12898983") {
            ob->add("combat_exp", time3*5/4);
            }

        ob->add("balance",gold*2);
        ob->add("combat_exp",exp/4);
        //tell_object(ob,"根据你的贡献度，你额外获得了"+gold*2/100+"两白银和"+exp/4+"点经验。"NOR"\n"); 

	  return "根据你的贡献度，额外奖励你"+gold*2/100+"两白银和"+exp/4+"点经验。"NOR"\n";
}

string ask_pay()
{
 
	object ob = this_player();

	if (ob->query("vip/all_pay")>0) {
		ob->add("zjvip/all_pay",ob->query("vip/all_pay"));
		ob->delete("vip/all_pay");
	}

	if (ob->query("zjvip/times") <1) return "我只处理信陵仙境的事情！\n";

	return "你当前贡献是"+chinese_number(ob->query("zjvip/all_pay")+ob->query("gongxians"))+"点。\n";
}

string ask_baotu()
{
	int now,last,nowfix,lastfix;
	mixed *t1,*t2;
	object tu,me = this_player();

	if (me->query("zjvip/times") <1) return "对不起，我只处理信陵仙境的事情！\n";
	if (me->query("zjvip/times") < 10*24*3600) return "对不起，会员时长已不足10天，不能领取藏波图！\n";
	last = me->query("zjvip/last_baotu");
	now = time();
	t1 = localtime(now);
	t2 = localtime(last);
	nowfix = now+(6-t1[6])*86400;
	lastfix = last+(6-t2[6])*86400;
	if(last&&(localtime(nowfix)[7]==localtime(lastfix)[7]))
		return "一周内只能领一次！\n";
	if ((me->query_encumbrance()*100 / me->query_max_encumbrance())>90)
		return "你身上的东西太多了！\n";
	if ( (time()-me->query("birthday")) < 7*86400 )
		return "你还是等下周再来领取藏宝图吧！\n";

	tu = new("/d/fuben/obj/heifeng1");
	tu->move(me);
	me->set("zjvip/last_baotu",now);
	message_vision("使者给了$N一张" + tu->query("name") + "。\n",me);
	log_file("vip_tu",ctime(time())+"："+me->query("name")+"在信陵君那里领取一张藏宝图。\n");
	return "这可是好东西，你抽空找朋友一起去探索一下吧。\n";
}

string ask_yao()
{
	object me = this_object();
	object ob = this_player();
        object yao;

	if (ob->query("zjvip/times") <1) return "对不起，我只处理信陵家族的事情！\n";

        if ( (time()-ob->query_temp("last_huxindan"))<1800 ) {
		    command("say 这位" + RANK_D->query_respect(ob) + "，你不是刚刚来领过天王护心丹吗，怎么又来要了？");
                ob->unconcious();
                return "真是讨打！";
            }

        if (  present("huxin dan", ob) )
                return RANK_D->query_respect(ob) + "你身上不是已经有天王护心丹了吗，怎么又来要了？ 真是贪得无厌！";

        if (objectp(present("huxin dan", environment(ob))))
                        return "曾经有一颗天王护心丹落你的眼前，你却没有好好珍惜...\n";

        if ((ob->query_encumbrance()*100 / ob->query_max_encumbrance())>90)
                return "你身上的东西太多了！\n";

        yao = new("/clone/shizhe/huxindan9");
        yao->set_amount(800);
        yao->move(ob);
        ob->set_temp("last_huxindan",time());
        message_vision("$N给了$n一些" + yao->query("name") + "。\n",me ,ob);
	return "这可是好东西，希望对你行走江湖有所帮助。\n";
}