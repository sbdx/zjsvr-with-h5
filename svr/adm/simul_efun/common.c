/**

common.c by mike/fang.j/大当家

仅限当前目录使用的公共方法文件
由于efun的加载顺序，只能这么干了

*/



#include <localtime.h>


// filter color
string filter_color(string arg)
{
#ifdef DOING_IMPROVED
	return efun::filter_ansi(arg);
#else
	arg = replace_string(arg, BLK, "");
	arg = replace_string(arg, RED, "");
	arg = replace_string(arg, GRN, "");
	arg = replace_string(arg, YEL, "");
	arg = replace_string(arg, BLU, "");
	arg = replace_string(arg, MAG, "");
	arg = replace_string(arg, CYN, "");
	arg = replace_string(arg, WHT, "");
	arg = replace_string(arg, HIR, "");
	arg = replace_string(arg, HIG, "");
	arg = replace_string(arg, HIY, "");
	arg = replace_string(arg, HIB, "");
	arg = replace_string(arg, HIM, "");
	arg = replace_string(arg, HIC, "");
	arg = replace_string(arg, HIW, "");
	arg = replace_string(arg, NOR, "");
	arg = replace_string(arg, BOLD, "");
	arg = replace_string(arg, BLINK, "");
	return arg;
#endif
}


string filter_ansi(string content)
{
    return filter_color(content);
}
