// dongfang.c
//Updated by y111

#include <ansi.h>
#include "baiyun.h"

inherit NPC;
inherit F_MASTER;
inherit F_COAGENT;
inherit F_QUESTER;

string ask_book();
mixed ask_pfm();

void create()
{
	set_name(RED"叶孤城"NOR, ({ "ye gucheng", "ye","gucheng" }) );
    set("nickname", HIY "剑神" NOR);
    set("gender", "男性");
    set("shen_type", 1);
    set("age", 33);
    set("long",
        "他就是剑神叶孤城，白云城城主。
\n");
    set("attitude", "peaceful");

    set("per", 21+random(100));
    set("str", 21+random(100));
    set("int", 30+random(100));
    set("con", 26+random(100));
    set("dex", 30+random(100));
	//自动PFM
	set("auto_perform", 1);

    set("inquiry", ([
        "夜孤城"   : "呵呵 \n",
        "白云城" : "百云孤城\n",
        //"葵花宝典" : (: ask_book :),
        //"绝招"     : (: ask_pfm :),
        //"鬼影"     : (: ask_pfm :),
    ]));

    set("kuihua_count", 1);

    set("qi", 500000);
    set("max_qi", 50000);
    set("jing", 10000);
    set("max_jing", 10000);
    set("neili", 45000);
    set("max_neili", 45000);
    set("jiali", 4000);

    set("combat_exp", 250000);
    set("score", 0);

    set_skill("parry", 300);
    set_skill("dodge", 300);
    set_skill("force", 300);
    set_skill("literate", 300);
	set_skill("sword", 300);
	set_skill("unarmed",300);
	set_skill("changquan",300);
	set_skill("feixian-steps",300);
	set_skill("feixian-sword",300);
	set_skill("jingyiforce",300);
    map_skill("dodge", "feixian-steps");
	map_skill("sword", "feixian-sword");
	map_skill("force", "jingyiforce");
	map_skill("parry", "feixian-sword");
	map_skill("unarmed", "changquan");
    create_family("白云城", 1, "城主");

    set("master_ob",5);
    
    set("chat_chance_combat", 60);
	set("chat_msg_combat", ({
		(: perform_action, "sword.bai" :),
		(: perform_action, "sword.luo" :),
		(: perform_action, "sword.se" :),
		(: perform_action, "sword.tianwai" :),
		(: exert_function, "recover" :),
		(: exert_function, "powerup" :),
	}));
    
    
	setup();
     carry_object("/clone/weapon/gangjian")->wield();
    //carry_object("/d/wanjiegu/npc/obj/qi-dress")->wear();
    carry_object("/d/heimuya/obj/yuxiao");
}

 

void attempt_apprentice(object ob)
{
  //   if(! permit_recruit(ob))  return;
     if((string)ob->query("family/master_name") == "叶孤城")
     {
          command("say 我越看你越不顺眼，快给我滚！");
          return;
     }
     if ((int)ob->query("combat_exp") < 600000)
     {
          command("say 就你这点微末功夫，还是先跟我徒弟多练练吧！");
          return;
     }

     if((int)ob->query_skill("feixian-steps", 1) < 160)
     {
          command("say 本门的内功轻身功法你还没练好，还要多下苦功才行！");
          return;
     }

     command("xixi");
     command("recruit " + ob->query("id")); 
     command("say 好！好！我白云城又多了一大助力。！");
     ob->set("title", HIR "白云亲传" NOR);
}

string ask_book()
{
     
     object ob;
     object me;

     me = this_player();
     if (me->query("family/family_name") != query
("family/family_name"))
            return "你这个" + RANK_D->query_rude(this_player()) +
                   "，打听本教秘笈有何图谋？";

     if (me->query("family/master_id") != query("id"))
            return "你又非我亲传弟子，打听宝典做什么？";

     if (me->query_skill("riyue-guanghua", 1) < 100)
            return "你的日月光华学得还不到家，贸然修炼宝典，有害无益。";

     if (query("kuihua_count") < 1)
            return "你来晚了，宝典我已经借出去了。";

     add("kuihua_count", -1);
     ob = new("/clone/book/kuihua");
     ob->move(me, 1);
     return "这本宝典你可要收好，弄丢了我找你算帐。";


}

void reset()
{
     set("kuihua_count", 1);
}

mixed ask_pfm()
{
     object me = this_player();

     if (me->query("can_perform/pixie-jian/ying"))
           return "我已指点于你，还多问什么？自己多练练去！";

     if (me->query("family/family_name") != query
("family/family_name"))
           return "本教武功独步武林，这位" + RANK_D->query_respect(me) 
+
                  "既然想学，不如入我日月神教如何？";

     if (me->query_skill("pixie-jian", 1) < 90)
           return "宝典所载武功博大精深，你连一半都没看完，多问什么？快
给我用功去！";

     if (me->query_skill("pixie-jian", 1) < 200)
           return "你的辟邪剑法尚欠火候，再多去钻研一下宝典！";

     message_vision(HIC "$n" HIC "对$N" HIC "点了点头：“很好，我来教你
一招，看仔细了。”\n" HIR
                    "只见$n" HIR "身形微晃，霎时不见踪影，$N" HIR
                    "只觉无数黑影在身边飘过，数道寒气逼向周身，全身一阵
刺痛。\n" NOR,
                    me, this_object());
     command("say 明白了没有？”"NOR);
     tell_object(me, HIW "你学会了「鬼影」这一招。\n" NOR);
     if (me->can_improve_skill("sword"))
             me->improve_skill("sword", 16000);
     me->set("can_perform/pixie-jian/ying", 1);
     return 1;
}
