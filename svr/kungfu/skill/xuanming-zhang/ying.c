#include <ansi.h>
#include <combat.h>
inherit F_SSERVER;
//玄冥神掌增加一个六连的招式
//修复六连招式不消化内力
#define YING "「" HIW "如影相随" NOR "」"

string query_name() { return YING; }
int perform(object me, object target)
{
        string msg;
        int ap, dp, i ,skill, delta;
        
        if (! target) target = offensive_target(me);

        if (! me->is_fighting(target))
                return notify_fail(YING "只能对战斗中的对手使用。\n");

        if( me->query_temp("weapon"))
                return notify_fail(YING "只能空手施展。\n");

      
        if ((int)me->query_skill("xuanming-zhang", 1) < 100)
                return notify_fail("你玄冥神掌不够娴熟，难以施展" YING "。\n");

        if (me->query_skill_mapped("strike") != "xuanming-zhang")
                return notify_fail("你没有激发玄冥神掌，难以施展" YING "。\n");

        if (me->query_skill_prepared("strike") != "xuanming-zhang")
                return notify_fail("你没有准备玄冥神掌，难以施展" YING "。\n");

        if (me->query_skill("dodge") < 180)
                return notify_fail("你的轻功修为不够，难以施展" YING "。\n");
        if( me->query("neili")<200 )
                return notify_fail("你现在的真气不够，难以施展" YING "。\n");
        if (! living(target))
	      return notify_fail("对方都已经这样了，用不着这么费力吧？\n");
                
        skill=(int)me->query_skill("xuanming-zhang", 1);
        msg = HIC "\n$N" HIC "长啸一声，施出绝招「" HIW "如影相随" HIC "」，"
              "双掌不断翻腾，掌风中透出阵阵阴寒之气，将$n" HIC "笼罩。\n" NOR;          
    message_combatd(msg, me, target);
   me->add("neili", -250);
    for (i = 0; i < 6; i++)
 {
		if (! me->is_fighting(target))
			break;
		COMBAT_D->do_attack(me, target, 0, 0);
	}

   me->start_busy(1 + random(6));
        return 1;
}
