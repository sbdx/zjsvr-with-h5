// tie@fengyun
// leiting.c 「雷霆一击」

#include <ansi.h>
#include <combat.h>

#define LEITING "「" HIY "九重寒天掌" NOR "」"

inherit F_SSERVER;

string query_name() { return "九重"ZJBR"寒天掌"; }
string *pfm_type() { return ({ "strike", }); }

int perform(object me, object target)
{
	int damage;
	string msg;
	int ap, dp;

	if (! target) target = offensive_target(me);
	
	if (! target || ! me->is_fighting(target))
		return notify_fail(LEITING "只能对战斗中的对手使用。\n");

	if ((int)me->query_skill("force") < 250)
		return notify_fail("你的内功火候不够，使不出" LEITING "。\n");

	if( (int)me->query("neili") < 500  ) 
		return notify_fail("你的内力不够。\n");
	if (me->query_temp("weapon") || me->query_temp("secondary_weapon"))
		return notify_fail("只能空手使用。\n");		

	if (! living(target))
		return notify_fail("对方都已经这样了，用不着这么费力吧？\n");

	msg = HIR  "$N使出［九重寒天掌］，全身飞速旋转，双掌一前一后，闪电般的击向$n！" NOR;

	ap = me->query_skill("strike");
	dp = target->query_skill("dodge") ;
	me->start_busy(3);
	if (ap / 2 + random(ap) > dp)
	{
		damage = ap + random(ap / 2);
		me->add("neili",-100);
		me->start_busy(1);
    msg += COMBAT_D->do_damage(me, target, UNARMED_ATTACK, damage, 60,HIR "$p" HIR "只见一阵旋风影中陡然现出$P"
					   HIR "的双掌，根本来不及躲避，被重重击中，\n寒冰"
					   "凌冽，让人招架不住！！！"NOR"\n");
	} else
	{
		msg += HIG "可是$p" HIG "看破了$P" HIG "的企图，没"
		       "有受到迷惑，闪在了一边。"NOR"\n";
	}
	message_combatd(msg, me, target);

	return 1;
}


