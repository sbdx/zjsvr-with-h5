inherit SKILL;
int difficult_level()
{
	return 1000;
}
string query_txt()
{
	return "要求：宗师"ZJBR
	"特效：圣兽青龙的灵技,每10级增加2%的命中,受灵-朱雀克制";
}
int valid_learn(object me)
{
    int level;
    level = me->query_skill("ling-qinglong", 1);

    if ((int)me->query_skill("martial-cognize", 1) < 300
        &&(int)me->query_skill("martial-cognize", 1) < level)
        return notify_fail("你觉得自己的武学修养有限，难以领会更高深的圣兽灵技。\n");

    if (me->query("character") != "狡黠多变")
        return notify_fail("你学了半天，发现青龙灵技，不合常理，古怪的很，根本无法领会。");

    return 1;
}