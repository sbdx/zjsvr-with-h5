inherit SKILL;
int difficult_level()
{
	return 1000;
}
string query_txt()
{
	return "要求：宗师"ZJBR
	"特效：圣兽朱雀的灵技,此次攻击未躲避时，有概率直接躲避，概率和等级成正比,受灵-青龙克制";
}
int valid_learn(object me)
{
    int level;
    level = me->query_skill("ling-zhuque", 1);

    if ((int)me->query_skill("martial-cognize", 1) < 300
        &&(int)me->query_skill("martial-cognize", 1) < level)
        return notify_fail("你觉得自己的武学修养有限，难以领会更高深的圣兽灵技。\n");

    if (me->query("character") != "阴险奸诈")
        return notify_fail("你看了朱雀灵技这阴险毒辣的武功，不"
                           "由的心惊肉跳，难以领会。\n");
    return 1;
}