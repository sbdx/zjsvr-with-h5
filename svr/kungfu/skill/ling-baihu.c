inherit SKILL;
int difficult_level()
{
	return 1000;
}
string query_txt()
{
	return "要求：宗师"ZJBR
	"特效：圣兽白虎的灵技,每10级增加2%的伤害,受灵-玄武克制";
}
int valid_learn(object me)
{
	int level;
	level = me->query_skill("ling-baihu", 1);

	if ((int)me->query_skill("martial-cognize", 1) < 300
    	&&(int)me->query_skill("martial-cognize", 1) < level)
		return notify_fail("你觉得自己的武学修养有限，难以领会更高深的圣兽灵技。\n");

	if (me->query("character") != "光明磊落")
		return notify_fail("白虎灵技正大恢弘，气度俨然，你怎么也学不得精髓。\n");
    return 1;
}