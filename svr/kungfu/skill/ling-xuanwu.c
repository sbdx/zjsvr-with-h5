inherit SKILL;
int difficult_level()
{
	return 1000;
}
string query_txt()
{
	return "要求：宗师"ZJBR
	"特效：圣兽玄武的灵技,每10级减少2%的受到伤害,受灵-白虎克制";
}
int valid_learn(object me)
{
    int level;
    level = me->query_skill("ling-xuanwu", 1);

    if ((int)me->query_skill("martial-cognize", 1) < 300
        &&(int)me->query_skill("martial-cognize", 1) < level)
        return notify_fail("你觉得自己的武学修养有限，难以领会更高深的圣兽灵技。\n");

    if (me->query("character") != "心狠手辣")
        return notify_fail("你发觉玄武灵技招招狠、式式险，舍"
                           "生忘死，自己实在领会不了。\n");
    return 1;
}