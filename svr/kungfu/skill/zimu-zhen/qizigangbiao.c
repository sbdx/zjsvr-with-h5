//七子钢镖伤害降低，叠毒能力提高
#include <ansi.h>
string query_name() { return "七子"ZJBR"钢镖"; }
#include <combat.h>
inherit F_SSERVER;
int perform(object me, object target)
{
	string msg;
	int extra,i,lmt,l;
	object weapon;
       if( !target ) target = offensive_target(me);
       if( !target
	||	!target->is_character()
	||	!me->is_fighting(target) )
		return notify_fail("[七子钢镖]只能对战斗中的对手使用。\n");
	if( (int)me->query_skill("biyun-xinfa", 1) < 60 )
                return notify_fail("你的碧云心法不够高。\n");
	  if (! objectp(weapon = me->query_temp("handing")) ||
            (string)weapon->query("id") != "gangbiao")
                return notify_fail("你现在手中没有拿着暗器钢镖，难以施展。\n");

        if( (int)me->query("neili", 1) < 400 )
                return notify_fail("你现在内力太弱。\n");

	weapon = me->query_temp("weapon");
	extra = me->query_skill("zimu-zhen",1);
	if ( extra < 200) return notify_fail("你的唐门暗器还不够纯熟！\n");
	msg = GRN  "$N使出唐门暗器中的七子钢镖，击向$n" NOR;
	me->add_temp("apply/attack", -150);
	me->add_temp("apply/damage", -150);

	message_vision(msg,me,target);
	 l=extra/30;
	lmt = random(l)+5;
	if(lmt>7) lmt=7;
	for(i=1;i<=lmt;i++)
	{
	msg =  BLU "第"+chinese_number(i)+"镖-->" NOR;
	COMBAT_D->do_attack(me,target, me->query_temp("weapon"),);
	}
if (extra > 200)
{
        target->apply_condition("chanchu_poison", 120);
        target->apply_condition("xiezi_poison", 120);
        target->apply_condition("wugong_poison", 120);
        target->apply_condition("snake_poison", 120);
        target->apply_condition("zhizhu_poison", 120);

}

if (extra > 300)
{
        target->apply_condition("tmzhuihun_poison", 120);
        target->apply_condition("tmpili_poison", 120);
        target->apply_condition("zm_poison", 120);
}
	if( random(me->query("combat_exp")) > (int)target->query("combat_exp")/3 ) {
        target->apply_condition("tmqidu_poison", 120);
	}
    me->add_temp("apply/attack", 150);
	me->add_temp("apply/damage", 150);

	me->start_busy(2+random(7));
	return 1;
}

