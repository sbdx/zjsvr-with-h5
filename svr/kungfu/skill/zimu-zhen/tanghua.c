#include <ansi.h>
//唐花连击数改为4练，伤害大幅度提高
string query_name() { return "唐花"; }
#include <combat.h>
inherit F_SSERVER;
int perform(object me, object target)
{
	string msg;
	int extra;
	object weapon;
	if( !target ) target = offensive_target(me);
	if( !target
	||	!target->is_character()
	||	!me->is_fighting(target) )
		return notify_fail("[唐花]只能对战斗中的对手使用。\n");
	weapon = me->query_temp("weapon");
	if( (int)me->query_skill("biyun-xinfa", 1) < 60 )
                return notify_fail("你的碧云心法不够高，不能运用「雨激梅花半瓣香」。\n");
	  if (! objectp(weapon = me->query_temp("handing")) ||
            (string)weapon->query("id") != "gangbiao")
                return notify_fail("你现在手中没有拿着暗器钢镖，难以施展。\n");
        if( (int)me->query("neili", 1) < 300 )
                return notify_fail("你现在内力太弱，不能使用「雨激梅花半瓣香」。\n");

    extra = me->query_skill("throwing") / 2;
	me->add_temp("apply/attack", extra*2);
	me->add_temp("apply/damage", extra*2);
  msg = RED "$N左手一扬，唐门独门暗器----一朵令人闻风丧胆的唐花朝"+target->query("name")+"发了出去。\n" NOR;
  message_vision(msg, me);
	/*COMBAT_D->do_attack(me,target, weapon,0,0);
	COMBAT_D->do_attack(me,target, weapon,0,0);
    COMBAT_D->do_attack(me,target, weapon,0,0);
    COMBAT_D->do_attack(me,target, weapon,0,0);*/

	if( random(me->query("combat_exp")) > (int)target->query("combat_exp")/2 ) {
        target->apply_condition("tmqidu_poison", 60);
	}
	me->add_temp("apply/attack", -extra*2);
	me->add_temp("apply/damage", -extra*2);
        me->add("neili",-50);
	me->start_busy(3);
	target->apply_condition("zimu-zhen",random(10)+60);

return 1;
}

