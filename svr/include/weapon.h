// weapon.h

#ifndef __WEAPON__
#define __WEAPON__

#define DEFAULT_WEAPON_LIMB "ÓÒÊÖ"

#define TWO_HANDED	1
#define SECONDARY	2
#define EDGED		4
#define POINTED	8
#define LONG		16

#define AXE		"/inherit/weapon/axe"		// ¸«
#define BLADE		"/inherit/weapon/blade"		// µ¶
#define CLUB		"/inherit/weapon/club"		// ¹÷
#define DAGGER		"/inherit/weapon/dagger"	// Ø°Ê×
#define FORK		"/inherit/weapon/fork"		// ²æ
#define HAMMER 		"/inherit/weapon/hammer"	// ´¸
#define PIN	     "/inherit/weapon/pin"	   // Õë
#define SWORD		"/inherit/weapon/sword"		// ½£
#define STAFF		"/inherit/weapon/staff"		// ÕÈ
#define THROWING	"/inherit/weapon/throwing"	// °µÆ÷
#define WHIP		"/inherit/weapon/whip"		// ±Þ
#define XSWORD	  "/inherit/weapon/xsword"	// Ïô

#define F_AXE		"/inherit/weapon/_axe"
#define F_BLADE		"/inherit/weapon/_blade"
#define F_CLUB		"/inherit/weapon/_club"
#define F_DAGGER	"/inherit/weapon/_dagger"
#define F_FORK		"/inherit/weapon/_fork"
#define F_HAMMER 	"/inherit/weapon/_hammer"
#define F_PIN	   "/inherit/weapon/_pin"
#define F_SWORD		"/inherit/weapon/_sword"
#define F_STAFF		"/inherit/weapon/_staff"
#define F_THROWING	"/inherit/weapon/_throwing"
#define F_WHIP		"/inherit/weapon/_whip"

#endif
