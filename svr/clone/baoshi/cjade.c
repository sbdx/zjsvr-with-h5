// cjade.c ôä´ä²ĞÆ¬

#include <ansi.h>
inherit COMBINED_ITEM;

void create()
{
	set_name(HIG "ôä´ä²ĞÆ¬" NOR, ({ "chipped jade" }) );
	if( clonep() )
		set_default_object(__FILE__);
	else {
		set("long", HIG "Ò»Æ¬¾§Ó¨ÌŞÍ¸Á£µÄôä´äËéÆ¬¡£"NOR"\n");
		set("base_value", 20000);
		set("base_unit", "Æ¬");
		set("base_weight", 1);
		set("can_be_enchased", __DIR__"jade");
		set("enchased_need", 3);
		set("magic/type", "cold");
		set("magic/power", 50);
	}
	set_amount(1);
	setup();
}
