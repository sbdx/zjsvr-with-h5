// mirror(fuben) npc
//by fang



#include <ansi.h>

inherit CLASS_D("generate") + "/family";
// 经验过高后无任何奖励
#define MAX_EXP              300000000
int  accept_object(object who, object ob);

string *arr_ref_msg=({
	"【$N哈哈长笑一声，运起内功，顿时精神好了起来！】",
	"【$N大喝一声，运用秘法，气血有所回升!】",
	"【$N激发全身潜力，满血复活！】",
	"【$N道：想过我这关？没那么容易！！】",
});



void create(string s)
{
	//自定义ID
	
	set("custom_id",s);
	::create();
	set("gender", "男性" );
	set("age", 30 + random(30));
	set("long", "");
	set("attitude", "friendly");
	//set("chat_chance", 3);
	//set("chat_msg", ({ (: random_move :) }));
	//set("chat_chance_combat", 5);
	
	set("no_get", 1);
	set("no_nuoyi",1);
	
	//自动PFM
	//set("auto_perform", 1);

	if (clonep()) {
		set_heart_beat(5);
		keep_heart_beat();
	}
}


//设置编号
void set_number(int i)
{
	set("number", i);
}

void set_from_me(object me)
{
	
	//武学休养
	set_skill("martial-cognize", 	500+random(100) );

}



// remove 对象被销毁时调用
void remove()
{
	::remove();
}



// 晕倒的时候有机会逃走
void unconcious()
{

	
	if (objectp(query_last_damage_from())){
		die( query_last_damage_from() );
	}else{
		die( );
	}
	
}

varargs void die(object killer)
{
	
	
	if ( query_temp("reborns") > 0 ){
		add_temp("reborns",-1);
		//full_self();
		message_vision( "$N" HIY "大喝一声，运用秘法，气血有所回升！"NOR"\n\n" , this_object());
		return;
	}
	
		(environment(this_object()))->cb_func();
		destruct( this_object() );
		
	
}

int accept_fight(object ob)
{
	command("say 好！咱们就比划比划！");
	kill_ob(ob);
	return 1;
}

int accept_hit(object ob)
{
	command("say 看招！");
	kill_ob(ob);
	return 1;
}

int accept_kill(object ob)
{
	command("say 哼！找死！");
	return 1;
}




void heart_beat(){
	

	
	object *env_obs,ob;
	int i;
	

	::heart_beat();
	
	
}

void do_help_me(object ob){
	return;
}



