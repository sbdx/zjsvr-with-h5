/**
 *  所有任务NPC的模型文件
 *  by fang q 184377367
 * 20161219
 */

#include <ansi.h>

inherit NPC;

void give_reward_forest(object,int);

object carry_object(string file)
{
	return ::carry_object(file);
}


mixed hit_ob(object me, object ob, int damage_bouns)
{
        ob->start_busy(5 + random(6));
        me->receive_wound("qi", 300 + random(300), ob);
        return HIY "$N" HIY "拼死反抗，竟逼得$n" HIY "手忙脚乱。\n" NOR;
}



void die(object killer)
{
	int proc=0;

	//调用子类方法
	give_reward_forest(killer,500+random(3000));
	
	
	  //1/20000几率掉铜人
        if ( random(20000) == 1 )
        {
                object ob_tongren;
                if( random(2) )
                {
                        ob_tongren = new("/clone/tongren/tongren1");
                }
                else{
                        ob_tongren = new("/clone/tongren/tongren2");
                }
                
                
                if (objectp(ob_tongren))
                {
                     message_vision(HIR "叮~~一声，从$N" HIR "身上掉出一样东西。\n" NOR, this_object());
                     ob_tongren->move(environment(this_object()));
                }
        }

	
	
	
	
	destruct( this_object() );
	
}






void unconcious(){
	if ( this_object()->query("qi") <= 0 || query("eff_qi") <= 0){
		
		if (objectp(query_last_damage_from())){
			die( query_last_damage_from() );
			return;
			//CHANNEL_D->do_channel(find_object(QUEST_D),"sys","这SB杀了我!" );
		}
		//CHANNEL_D->do_channel(find_object(QUEST_D),"sys","在晕死中被销毁....." );
		//log_file("test.c","unc"+file_name(environment())+"\n");
		destruct( this_object() );

	}
	
	
}







