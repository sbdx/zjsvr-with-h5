
#include <ansi.h>
inherit "/inherit/char/rhizotomist";


void create()
{
	set_name("采药郎中", ({ "caiyaolangzhong", "caiyaolangzhong" }));
	set("title", "大脚郎中");
	

	set("gender", "男性");
	set("age", 29);
	set("long",
		"这个郎中看起来精神很好,长年在外采集炼药药材\n");
	set_skill("unarmed", 20);
	set_skill("dodge", 20);
	set_temp("apply/damage",100);
	//战斗经验？
	set("combat_exp", 4000);
	//attitude 态度 friendly 友好的..
	set("attitude", "friendly");
	
	setup();
	carry_object("/clone/misc/cloth")->wear();
}

varargs int receive_damage(string type, int n, object who)
{
	if (! objectp(who) || ! interactive(who) || who == this_object())
		return 0;

	tell_object(who, HIR "你发现" + name() + HIR "诡秘一笑，忽然觉得一阵心悸，神智一阵恍惚..."NOR"\n");
	return who->receive_damage(type, n, this_object());
}

varargs int receive_wound(string type, int n, object who)
{
	if (! objectp(who) || ! interactive(who) || who == this_object())
		return 0;

	tell_object(who, RED "你忽见" + name() + RED "桀桀怪笑，只觉得浑身都是一痛，直入骨髓深处..."NOR"\n");
	return who->receive_wound(type, n, this_object());
}
