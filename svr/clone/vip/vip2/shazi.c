#include <ansi.h>

inherit COMBINED_ITEM;//这里要改


void create()
{
	set_name(HIR "我是傻子" NOR, ({ "shazi dan"}) );
	if( clonep() )
		set_default_object(__FILE__);
	else {
		set("long", "吃过之后内删除读书写字\n");
		set("spectxt", "吃过之后内删除读书写字\n");
        set("no_sell", 1);
		set("yuanbao", 200);
	    set("base_value", 500);//药物属性前缀要改
		set("base_unit", "颗");
		set("base_weight", 30);
		set("only_do_effect", 1);
		set("no_shop", 1);
		set_amount(1);//增加数量
	}
}

int do_effect(object me)
{
	mapping my;
      if (me->is_fighting()) 
		return notify_fail("你现在正在战斗！\n");

	message_vision("$N一仰脖，吞下了一颗" + this_object()->name() +"。\n", me);

	me->delete_skill("literate");
	me->start_busy();
	add_amount(-1);//效果后减少该数量
	return 1;
}
string query_autoload() { return query_amount() + ""; }//叠加物品保存
void autoload(string param)//还要加这个
{
        int amt;

        if (sscanf(param, "%d", amt) == 1)
                set_amount(amt);
}