// cybs_cloth.c

#include <ansi.h>
#include <armor.h>

inherit BOOTS;

string do_wear();

void create()
{
	set_name(HIY "赤焰青铜靴" NOR, ({"cyq boots"}));
	set_weight(300);
	if( clonep() )
		set_default_object(__FILE__);
	else {
		set("unit", "双");
		set("long", "这是一件"+this_object()->query("name")+"，是代表武林大会首届冠军的套装，彰显着身为王者无上的荣耀。。\n");
		set("material", "boots");    //类型
		set("armor_prop/armor", 5);  //防御
		set("armor_prop/warm", 3);   //保暖
                set("armor_prop/int", 1);
		set("value", 1000);   //价值
		set("wear_msg", (: do_wear :));  //装备信息
      //---------------------------------------------------------------
	      //套装参数
		set("suit",HIG"赤焰青铜套装"NOR);  //套装名称
		set("suit_lvl",5);	     //套装等级
		set("suit_count",3);	   //套装部件数量
	      //套装各部件
		set("suit_mod/cloth","赤焰青铜甲");
		set("suit_mod/boots","赤焰青铜靴");
		set("suit_mod/pants","赤焰青铜裤");
	       //套装全部附加天赋效果  
		set("suit_eff/strength",5);      //两件
		set("suit_eff/intelligence",5);      //三件  
		set("suit_eff/spirituality",5);      //    一套
		set("suit_eff/constitution",5);      //三件  
		//套装全部附加技能效果     
	       set("suit_eff_skill/force",10);   
       //------------------------------------------------------
	}
	setup();
}

string do_wear()
{
	object me;
	string msg;
	int per;

	me = this_player();
	per = me->query("per");
	if (me->query("gender") == "女性")
	{
		if (per >= 30)
			msg = HIC "$N" HIC "轻轻将一件$n" HIC "穿在脚上，神态曼妙之极。\n";
		else if (per >= 25)
			msg = HIG "$N" HIG "把$n" HIG "展开，穿在脚上。\n";
		else if (per >= 20)
			msg = YEL "$N" YEL "把$n" YEL "穿在脚上，缩了缩脖子。\n";
		else    msg = YEL "$N" YEL "毛手毛脚的把$n" YEL "穿在脚上。\n";
	} else
	{
		if (per >= 30)
			msg = HIC "$N" HIC "随手一挥，将$n" HIC "穿在脚上，姿势潇洒之极。\n";
		else if (per >= 25)
			msg = HIG "$N" HIG "把$n" HIG "展开，穿在脚上。\n";
		else if (per >= 20)
			msg = YEL "$N" YEL "把$n" YEL "穿在脚上，扯了扯衣角，缩了缩脖子。\n";
		else    msg = YEL "$N" YEL "毛手毛脚的把$n" YEL "穿在脚上，甚是猥琐。\n";
	}
	return msg;
}

int query_autoload()
{
	return 1;
}
