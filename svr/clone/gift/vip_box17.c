
#include <ansi.h>

inherit COMBINED_ITEM;

string query_autoload() { return query_amount() + ""; }

void autoload(string param)
{
	int amt;

	if (sscanf(param, "%d", amt) == 1)
		set_amount(amt);
}

void setup()
{
	set_amount(1);
	::setup();
}

void create()
{
	set_name("VIP17宝盒", ({"vip17 gift"}));
	if (clonep())
		set_default_object(__FILE__);
	else {
		set("long", "信陵君一路走来感谢有您，祝您游戏愉快！\n");
		set("base_unit", "个");
		set("base_weight", 1);
		set("base_value", 1000);
		set("no_put", 1);
		set("no_get", 1);
		set("no_give", 1);
		set("no_drop", 1);
		set("no_sell", 1);
		set("no_steal", 1);
		set("no_beg", 1);
		set("only_do_effect", 1);
	}
	setup();
}

int do_effect(object me)
{
	int i;
	mapping gifts;
	string *list;
	object obj;

	gifts = ([
		"/clone/vip/vip2/putao1":5,		//5神奇葡萄
		"/clone/gift/tianling":5,		    //5天灵
		"/clone/gift/book_wuliang":5,	    //5无量经书
		"/clone/shizhe/jinsha":5,	        //5金沙
		"/clone/gift/lingzhi":1,		    //1灵芝
		"/d/fuben/obj/heifeng1":5,		    //5藏宝图		
		"/clone/vip/vip2/tianxiang":5,     //5天香玉露
		"/clone/vip/vip2/dan_jiuzhuan":5,  //5九转金丹
		//"/clone/gift/god_eyes":5,   //5上帝之眼
		"/clone/vip/ling_wolong":5,        //5卧龙令
		//"/clone/shizhe/tianling2":2,        //2玄灵丹
	]);

	list = keys(gifts);

	if (me->is_busy())
		return notify_fail("你正忙着呢。\n");

	if (me->is_fighting())
		return notify_fail("战斗中，不能使用"+ name() +"。\n");

	message_vision("$N打开了一" + query("base_unit") + name() + "。\n", me);
	me->add("vip_box_times",1);
	log_ufile(me,"vipbox","第"+me->query("vip_box_times")+"次打开了"+this_object()->short()+"。\n");
	for(i=0;i<sizeof(list);i++)
	{
		obj = new(list[i]);
		if(obj->query_amount())
			obj->set_amount(gifts[list[i]]);
		tell_object(me,"你发现了"+obj->short()+"。\n");
		obj->move(me);
	}
	add_amount(-1);

	return 1;

}
