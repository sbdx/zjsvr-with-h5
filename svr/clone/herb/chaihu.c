// ruxiang.c

#include "herb.h"

void create()
{
	set_name(CYN "²ñºú" NOR, ({ "chai hu", "herb_chaihu" }));
	if (clonep())
		set_default_object(__FILE__);
	else {
		set("long", "ÕâÊÇÖÐÒ©Ò©²Ä¡£\n");
		set("base_unit", "¿é");
		set("base_value", 4000);
		set("base_weight", 60);
	}
	setup();
}
