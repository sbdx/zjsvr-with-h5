#include <ansi.h>
#include <armor.h>

//inherit CLOTH;
inherit FINGER;
string do_wear();
void create()
{
        set_name( HIW "风来・" HIY "天使联盟戒" NOR, ({ "fenglai jie" }) );
        set_weight(10);
        if( clonep() )
                set_default_object(__FILE__);
        else {
                set("unit", "件");
                set("long", HIW "传说中天下第一武神风来的配饰，风来・天使联盟戒里蕴含着伟岸的神力和无上的魅力。\n");
                set("xy_money", 10000);   
                set("wear_msg", (: do_wear :));  //装备信息
                set("suit",HIG"天使联盟套装"NOR);  //套装名称
              	set("suit_lvl",5);	     //套装等级
                set("suit_count",1);	   //套装部件数量             
				set("suit_eff/intelligence",30); //后天悟性+30
				set("suit_eff/constitution",30); //后天根骨+30
				set("suit_eff_skill/taiji-jian", 200);   
				set("suit_eff_skill/taiji-quan", 200);
				set("suit_eff_skill/taoism", 200);
			  //set("material", "armor");    
				set("material", "finger");
				set("armor_prop/armor", 666);  //防御
        }
        setup();
}

int query_autoload()
{
        return 1;
}

string do_wear()
{
	object me;
	string msg;
	int per;

	me = this_player();
	per = me->query("per");
	if (me->query("gender") == "女性")
	{
		if (per >= 30)
			msg = HIC "$N" HIC "轻轻将一件$n" HIC "披在身上，神态曼妙之极。\n";
		else if (per >= 25)
			msg = HIG "$N" HIG "把$n" HIG "展开，披在身上。\n";
		else if (per >= 20)
			msg = YEL "$N" YEL "把$n" YEL "披在身上，缩了缩脖子。\n";
		else    msg = YEL "$N" YEL "毛手毛脚的把$n" YEL "披在身上。\n";
	} else
	{
		if (per >= 30)
			msg = HIC "$N" HIC "随手一挥，将$n" HIC "披在身上，姿势潇洒之极。\n";
		else if (per >= 25)
			msg = HIG "$N" HIG "把$n" HIG "展开，披在身上。\n";
		else if (per >= 20)
			msg = YEL "$N" YEL "把$n" YEL "披在身上，扯了扯衣角，缩了缩脖子。\n";
		else    msg = YEL "$N" YEL "毛手毛脚的把$n" YEL "披在身上，甚是猥琐。\n";
	}
	if (me->query("id")=="lias3"/*||me->query("id")=="baiyi666"*/)
	{
	if (me->query("id")=="lias3")
	write("识别人物为风来了，识别通过\n");
	/*else
	if (me->query("id")=="baiyi666")
	write("识别人物为潇湘夜雨，识别通过\n");*/
	else
	return msg;
	me->set_temp("apply/int",20);
	me->set_temp("apply/con",20);
	}
	return msg;
}
