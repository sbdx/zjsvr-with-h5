//by 游侠 ranger's ednpc tools.
// shanzei1.c

#include <ansi.h>
inherit NPC;

string *random_ob = ({
  "/fuben/wuguan/obj/putao",
    "/fuben/wuguan/obj/dan_chongmai2",
    "/fuben/wuguan/obj/book_wuliang",    
});

void create()
{
object ob = this_player();
int lvl = ob->query("max_qi");

if (lvl > 10000){
lvl = 10000;
}
	set_name(HIM"元兵队长"NOR,({"duizhang"}));
	set("gender", "男性");
	set("per",20);
	set("age", 30);
	set("combat_exp", 1000000+random(3000000));
	set("str", 30);
	set("int", 20);
	set("con", 20);
	set("dex", 20);
	
	set("max_neili", lvl);
	set("neili", lvl);
	set("eff_qi", lvl);
	set("max_qi", lvl);
	set("qi", lvl);
	set("max_jing", lvl);
	set("jing", lvl);
	set("max_jingli", lvl);
    set("eff_jingli", lvl);
 	set("jingli", lvl);
 	set("jiali", 100);
	set_temp("apply/armor", 350);
	set_temp("apply/damage", 150);
			
	set_skill("unarmed", 270);
	set_skill("force", 270);
	set_skill("club", 270);
	set_skill("zhongping-qiang", 270);
	set_skill("dodge", 270);
	set_skill("parry", 270);
	map_skill("club", "zhongping-qiang");
	
	setup();
	carry_object("/d/city/npc/obj/tiejia")->wear();
	carry_object("/clone/weapon/changqiang")->wield();
}

void die()
{
	object killer,ob;
	string id = random_ob[random(sizeof(random_ob))];//随机物品
	int exp;
	int num;
	if (objectp(killer = query_last_damage_from()))
	{
	    num=50+random(50);	
		killer->add("combat_exp",4500);
		killer->add("potential",3000);
	    killer->add("yuanbao_2",num);		
		tell_object(killer,"你杀死"+name()+"，获得6000点经验，4500点潜能"+num+"元宝票。\n");
	}
	switch (random(10))
	{
		case 0 : 
		case 5 :
		case 9 :
		new(id)->move(killer);
		new(id)->move(killer);
		new(id)->move(killer);
CHANNEL_D->do_channel(this_object(),"rumor", killer->query("name")+"在单骑破元副本中获得了"+id->short()+"。");									
		break;
	}
	::die();
}
void unconcious()
{
	die();
}