//by 游侠 ranger's ednpc tools.
// shanzei1.c

#include <ansi.h>
inherit NPC;

string *random_ob = ({
  "/fuben/wuguan/obj/jinsha",
    "/fuben/wuguan/obj/hua4",
    "/fuben/wuguan/obj/xiaohuan-dan",   
	"/fuben/wuguan/obj/dan_chongmai1",
});

void create()
{
object ob = this_player();
int lvl = ob->query("max_qi");

if (lvl > 5000){
lvl = 5000;
}
	set_name("蒙古兵",({"bing"}));
	set("gender", "男性");
	set("per",20);
	set("age", 30);
	set("combat_exp", 1000000+random(3000000));
	set("str", 20);
	set("int", 20);
	set("con", 20);
	set("dex", 20);
	
	set("max_neili", lvl);
	set("neili", lvl);
	set("eff_qi", lvl);
	set("max_qi", lvl);
	set("qi", lvl);
	set("max_jing", lvl);
	set("jing", lvl);
	set("max_jingli", lvl);
    set("eff_jingli", lvl);
 	set("jingli", lvl);
	set_temp("apply/damage", 150);
	
	set_skill("unarmed", 250);
	set_skill("force", 250);
	set_skill("club", 250);
	set_skill("zhongping-qiang", 250);
	set_skill("dodge", 250);
	set_skill("parry", 250);
	map_skill("club", "zhongping-qiang");
	
	setup();
	carry_object("/d/city/npc/obj/tiejia")->wear();
	carry_object("/clone/weapon/changqiang")->wield();
}

void die()
{
	string id = random_ob[random(sizeof(random_ob))];//随机物品
	object killer,ob;
	int exp;

	if (objectp(killer = query_last_damage_from()))
	{
		killer->add("combat_exp",3600);
		killer->add("potential",2400);
		tell_object(killer,"你杀死"+name()+"，获得3600点经验，2400点潜能。\n");
	}
	
	switch (random(10))
	{
		case 0 : 
		case 5 :
		case 9 :
		new(id)->move(killer);
		new(id)->move(killer);
		new(id)->move(killer);
CHANNEL_D->do_channel(this_object(),"rumor", killer->query("name")+"在单骑破元副本中获得了"+id->short()+"。");									
		break;
	}
	::die();
}

void unconcious()
{
	die();
}