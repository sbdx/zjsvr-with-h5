//by 游侠 ranger's ednpc tools.
// shanzei1.c

#include <ansi.h>
inherit NPC;

string *random_ob = ({
  "/fuben/wuguan/obj/putao",
    "/fuben/wuguan/obj/dan_chongmai2",
    "/fuben/wuguan/obj/book_wuliang",    
});

void create()
{
object ob = this_player();
int lvl = ob->query("max_qi");


if (lvl > 15000){
lvl = 15000;
}
	set_name(HIM"元兵统领"NOR,({"tongling"}));
	set("gender", "男性");
	set("per",20);
	set("age", 30);
	set("combat_exp", 1000000+random(3000000));
	set("str", 50);
	set("int", 20);
	set("con", 20);
	set("dex", 20);
	
	set("max_neili", lvl);
	set("neili", lvl);
	set("eff_qi", lvl);
	set("max_qi", lvl);
	set("qi", lvl);
	set("max_jing", lvl);
	set("jing", lvl);
	set("max_jingli", lvl);
    set("eff_jingli", lvl);
 	set("jingli", lvl);
 	set("jiali", 200); 	
 	set_temp("apply/attack", 300);
	set_temp("apply/armor", 350);
	set_temp("apply/damage", 150); 	
		
	set_skill("unarmed", 320);
	set_skill("force", 320);
	set_skill("club", 320);
	set_skill("zhongping-qiang", 320);
	set_skill("dodge", 320);
	set_skill("parry", 320);
	map_skill("club", "zhongping-qiang");
	
	setup();
	carry_object("/d/city/npc/obj/tiejia")->wear();
	carry_object("/clone/weapon/changqiang")->wield();
}

void die()
{
	object killer,ob;
	int exp;
	int num;
	if (objectp(killer = query_last_damage_from()))
	{
	    num=50+random(50);	
		killer->add("combat_exp",6000);
		killer->add("potential",4500);
	    killer->add("yuanbao_2",num);		
		tell_object(killer,"你杀死"+name()+"，获得6000点经验，4500点潜能"+num+"元宝票。\n");
	}
	::die();
}
void unconcious()
{
	die();
}