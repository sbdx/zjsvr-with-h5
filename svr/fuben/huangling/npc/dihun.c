//by 游侠 ranger's ednpc tools.
// shanzei1.c

#include <ansi.h>
inherit NPC;

string *random_ob = ({
      "/fuben/wuguan/obj/suip1",
    "/fuben/wuguan/obj/suip2",
    "/fuben/wuguan/obj/suip3",  
	"/fuben/wuguan/obj/dan_chongmai1",
});

void create()
{
object ob = this_player();
int lvl = ob->query("max_qi");
int sklv = to_int(pow(to_float(ob->query("combat_exp")*10),0.333333333)+random(15));
if (lvl > 7000){
lvl = 7000*4;
}
	set_name(HIY"帝魂"NOR,({"di hun"}));
	set("gender", "男性");
	set("per",20);
	set("age", 30);
	set("combat_exp", 9999999+random(3000000));
	set("str", 20);
	set("int", 20);
	set("con", 20);
	set("dex", 20);
	
	set("max_neili", lvl);
	set("neili", lvl);
	set("eff_qi", lvl);
	set("max_qi", lvl);
	set("qi", lvl);
	set("max_jing", lvl);
	set("jing", lvl);
	set("max_jingli", lvl);
    set("eff_jingli", lvl);
 	set("jingli", lvl);
 	set("jiali", 50); 	
    set_temp("apply/attack", 300);
	set_temp("apply/armor", 350);
	set_temp("apply/damage", 150); 			
	set_skill("force", sklv);
	set_skill("dodge", sklv);
	set_skill("hand", sklv);
	set_skill("cuff", sklv);	
	set_skill("parry", sklv);
    set_skill("longxiang", sklv);	
    set_skill("shenkong-xing", sklv);
    set_skill("dashou-yin", sklv);	
    set_skill("yujiamu-quan", sklv);	        	    	
	
	map_skill("force", "longxiang");
	map_skill("dodge", "shenkong-xing");
	map_skill("hand", "dashou-yin");
	map_skill("cuff", "yujiamu-quan");	
	map_skill("parry", "dashou-yin");			

	prepare_skill("hand", "dashou-yin");
	prepare_skill("cuff", "yujiamu-quan");

	set("chat_chance_combat", 80);
	set("chat_msg_combat", ({
		(: perform_action, "hand.jingang" :),
		(: perform_action, "cuff.chen" :),
		(: exert_function, "powerup" :),
		(: exert_function, "recover" :),
	}));
	setup();
}

void die()
{
	string id = random_ob[random(sizeof(random_ob))];//随机物品
	object killer,ob;
	int exp;

	if (objectp(killer = query_last_damage_from()))
	{
		killer->add("experience",15000);
		tell_object(killer,"你杀死"+name()+"，获得15000点实战体会。\n");
	}
	
	switch (random(10))
	{
		case 0 : 
		case 5 :
		case 9 :
			new(id)->move(killer);
CHANNEL_D->do_channel(this_object(),"rumor", killer->query("name")+"在十三皇陵中获得了"+id->short()+"。");									
		break;
	}
	::die();
}

void unconcious()
{
	die();
}