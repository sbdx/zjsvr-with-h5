//by 游侠 ranger's ednpc tools.
// shanzei1.c

#include <ansi.h>
inherit NPC;

string *random_ob = ({
    "/fuben/wuguan/obj/putao",
    "/fuben/wuguan/obj/dan_chongmai1",
    "/fuben/wuguan/obj/tianling",    
});

void create()
{
object ob = this_player();
int lvl = ob->query("max_qi");//判定玩家最大气血

if (lvl > 5000){
lvl = 5000;//最大气血不能超过5000
}
	set_name(HIY"武馆弟子"NOR,({"wuguan dizi"}));
	set("gender", "男性");
	set("per",20);
	set("age", 30);
	set("combat_exp", 1000000+random(3000000));
	set("str", 20);
	set("int", 20);
	set("con", 20);
	set("dex", 20);
	
	set("max_neili", lvl);
	set("neili", lvl);
	set("eff_qi", lvl);
	set("max_qi", lvl);
	set("qi", lvl);
	set("max_jing", lvl);
	set("jing", lvl);
	set("max_jingli", lvl);
    set("eff_jingli", lvl);
 	set("jingli", lvl);
		
	set_skill("force", 150);
	set_skill("dodge", 150);
	set_skill("unarmed", 150);
	set_skill("parry", 150);
    set_skill("shaolin-xinfa", 150);	
    set_skill("shaolin-shenfa", 150);
    set_skill("changquan", 150);	    	    	
	
	map_skill("force", "shaolin-xinfa");
	map_skill("dodge", "shaolin-shenfa");
	map_skill("unarmed", "chanquan");
	map_skill("parry", "chanquan");		
	setup();
}

void die()
{
	string id = random_ob[random(sizeof(random_ob))];//随机物品
	object killer,ob;
	int exp;

	if (objectp(killer = query_last_damage_from()))
	{
		killer->add("combat_exp",4000);
		killer->add("potential",2000);
		tell_object(killer,"你杀死"+name()+"，获得4000点经验，2000点潜能。\n");
	}
	
	switch (random(10))
	{
		case 0 : 
		case 5 :
		case 9 :
		new(id)->move(killer);
		new(id)->move(killer);
		new(id)->move(killer);
CHANNEL_D->do_channel(this_object(),"rumor", killer->query("name")+"在武馆训练中获得了"+id->short()+"。");									
		break;
	}
	::die();
}

void unconcious()
{
	die();
}