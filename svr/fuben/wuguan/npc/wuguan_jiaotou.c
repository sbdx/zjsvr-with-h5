//by 游侠 ranger's ednpc tools.
// shanzei1.c

#include <ansi.h>
inherit NPC;
string *random_ob = ({
    "/fuben/wuguan/obj/book_tianfu",
    "/fuben/wuguan/obj/book_wuliang",
	"/fuben/wuguan/obj/dan_chongmai1",
    
});
void create()
{
object ob = this_player();
int lvl = ob->query("max_qi");
if (lvl > 15000){
lvl = 15000;
}
	set_name(HIM"教头"NOR,({"wuguan jiaotou"}));
	set("gender", "男性");
	set("per",20);
	set("age", 30);
	set("combat_exp", 1000000+random(3000000));
	set("str", 20);
	set("int", 20);
	set("con", 20);
	set("dex", 20);
	
	set("max_neili", lvl);
	set("neili", lvl);
	set("eff_qi", lvl);
	set("max_qi", lvl);
	set("qi", lvl);
	set("max_jing", lvl);
	set("jing", lvl);
	set("max_jingli", lvl);
    set("eff_jingli", lvl);
 	set("jingli", lvl);
		
	set_skill("force", 180);
	set_skill("dodge", 180);
	set_skill("unarmed", 180);
	set_skill("parry", 180);
    set_skill("shaolin-xinfa", 180);	
    set_skill("shaolin-shenfa", 180);
    set_skill("changquan", 180);	    	    	
	
	map_skill("force", "shaolin-xinfa");
	map_skill("dodge", "shaolin-shenfa");
	map_skill("unarmed", "chanquan");
	map_skill("parry", "chanquan");		
	setup();
}

void die()
{
	object killer,ob;
	int exp;
	int num;
	string id = random_ob[random(sizeof(random_ob))];//随机物品
	if (objectp(killer = query_last_damage_from()))
	{
	    num=50+random(50);	
		killer->add("combat_exp",2400);
		killer->add("potential",1200);
	    killer->add("yuanbao_2",num);		
		tell_object(killer,"你杀死"+name()+"，获得8000点经验，4000点潜能"+num+"元宝票。\n");
	}
	switch (random(10))
	{
		case 0 : 
		case 5 :
		case 9 :
		new(id)->move(killer);
		new(id)->move(killer);
		CHANNEL_D->do_channel(this_object(),"rumor", killer->query("name")+"在武馆训练中获得了"+id->short()+"。");									
		break;
	}
	::die();
}
void unconcious()
{
	die();
}
