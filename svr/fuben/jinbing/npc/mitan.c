//by 游侠 ranger's ednpc tools.
// shanzei1.c

#include <ansi.h>
inherit NPC;

string *random_ob = ({
  "/fuben/wuguan/obj/dan_chongmai1",
    "/fuben/wuguan/obj/fushougao",
    "/fuben/wuguan/obj/jinsha",   
	"/fuben/wuguan/obj/dan_chongmai1",
});

void create()
{
object ob = this_player();
int lvl = ob->query("max_qi");
if (lvl > 5000){
lvl = 5000;
}

	set_name(HIY"金国密探"NOR,({"tan zi"}));
	set("gender", "男性");
	set("per",20);
	set("age", 30);
	set("combat_exp", 1000000+random(3000000));
	set("str", 20);
	set("int", 20);
	set("con", 20);
	set("dex", 20);
	
	set("max_neili", lvl);
	set("neili", lvl);
	set("eff_qi", lvl);
	set("max_qi", lvl);
	set("qi", lvl);
	set("max_jing", lvl);
	set("jing", lvl);
	set("max_jingli", lvl);
    set("eff_jingli", lvl);
 	set("jingli", lvl);
		
	set_skill("force", 200);
	set_skill("dodge", 200);
	set_skill("hand", 200);
	set_skill("cuff", 200);	
	set_skill("parry", 200);
    set_skill("mizong-neigong", 200);	
    set_skill("shenkong-xing", 200);
    set_skill("dashou-yin", 200);	
    set_skill("yujiamu-quan", 200);	        	    	
	
	map_skill("force", "mizong-neigong");
	map_skill("dodge", "shenkong-xing");
	map_skill("hand", "dashou-yin");
	map_skill("cuff", "yujiamu-quan");	
	map_skill("parry", "dashou-yin");	
	
	prepare_skill("hand", "dashou-yin");
	prepare_skill("cuff", "yujiamu-quan");
	setup();
}

void die()
{
	string id = random_ob[random(sizeof(random_ob))];//随机物品
	object killer,ob;
	int exp;

	if (objectp(killer = query_last_damage_from()))
	{
		killer->add("combat_exp",2400);
		killer->add("potential",1800);
		tell_object(killer,"你杀死"+name()+"，获得2400点经验，1800点潜能。\n");
	}
	
	switch (random(10))
	{
		case 0 : 
		case 5 :
		case 9 :
			new(id)->move(killer);
		   new(id)->move(killer);
		   new(id)->move(killer);
CHANNEL_D->do_channel(this_object(),"rumor", killer->query("name")+"在伏击金兵副本中获得了"+id->short()+"。");									
		break;
	}
	::die();
}

void unconcious()
{
	die();
}