//by 游侠 ranger's ednpc tools.
// shanzei1.c

#include <ansi.h>
inherit NPC;

string *random_ob = ({
     "/fuben/wuguan/obj/putao",
    "/fuben/wuguan/obj/dan_chongmai1",
    "/fuben/wuguan/obj/book_wuliang",    
});

void create()
{
object ob = this_player();
int lvl = ob->query("max_qi");
if (lvl > 15000){
lvl = 15000;
}
	set_name(HIR"金国首领"NOR,({"shouling"}));
	set("gender", "男性");
	set("per",20);
	set("age", 30);
	set("combat_exp", 9999999+random(3000000));
	set("str", 20);
	set("int", 20);
	set("con", 20);
	set("dex", 20);
	
	set("max_neili", lvl);
	set("neili", lvl);
	set("eff_qi", lvl);
	set("max_qi", lvl);
	set("qi", lvl);
	set("max_jing", lvl);
	set("jing", lvl);
	set("max_jingli", lvl);
    set("eff_jingli", lvl);
 	set("jingli", lvl);
 	set("jiali", 50); 	
		
	set_skill("force", 280);
	set_skill("dodge", 280);
	set_skill("hand", 280);
	set_skill("cuff", 280);	
	set_skill("parry", 280);
    set_skill("longxiang", 280);	
    set_skill("shenkong-xing", 280);
    set_skill("dashou-yin", 280);	
    set_skill("yujiamu-quan", 280);	        	    	
	
	map_skill("force", "longxiang");
	map_skill("dodge", "shenkong-xing");
	map_skill("hand", "dashou-yin");
	map_skill("cuff", "yujiamu-quan");	
	map_skill("parry", "dashou-yin");			

	prepare_skill("hand", "dashou-yin");
	prepare_skill("cuff", "yujiamu-quan");

	set("chat_chance_combat", 80);
	set("chat_msg_combat", ({
		(: perform_action, "hand.jingang" :),
		(: perform_action, "cuff.chen" :),
		(: exert_function, "powerup" :),
		(: exert_function, "recover" :),
	}));
	setup();
}

void die()
{
	object killer,ob;
	int exp;
	int num;
	if (objectp(killer = query_last_damage_from()))
	{
	    num=50+random(50);	
		killer->add("combat_exp",4500);
		killer->add("potential",3000);
	    killer->add("yuanbao_2",num);		
		tell_object(killer,"你杀死"+name()+"，获得4500点经验，3000点潜能"+num+"元宝票。\n");
	}
	::die();
}
void unconcious()
{
	die();
}