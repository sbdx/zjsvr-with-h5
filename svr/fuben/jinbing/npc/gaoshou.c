//by 游侠 ranger's ednpc tools.
// shanzei1.c

#include <ansi.h>
inherit NPC;

string *random_ob = ({
   "/fuben/wuguan/obj/putao",
    "/fuben/wuguan/obj/book_wuliang",    
	"/fuben/wuguan/obj/dan_chongmai1",
});

void create()
{
object ob = this_player();
int lvl = ob->query("max_qi");
if (lvl > 10000){
lvl = 10000;
}
	set_name(HIM"金国高手"NOR,({"gaoshou"}));
	set("gender", "男性");
	set("per",20);
	set("age", 30);
	set("combat_exp", 1000000+random(3000000));
	set("str", 20);
	set("int", 20);
	set("con", 20);
	set("dex", 20);
	
	set("max_neili", lvl);
	set("neili", lvl);
	set("eff_qi", lvl);
	set("max_qi", lvl);
	set("qi", lvl);
	set("max_jing", lvl);
	set("jing", lvl);
	set("max_jingli", lvl);
    set("eff_jingli", lvl);
 	set("jingli", lvl);
		
	set_skill("force", 225);
	set_skill("dodge", 225);
	set_skill("hand", 225);
	set_skill("cuff", 225);	
	set_skill("parry", 225);
    set_skill("longxiang", 225);	
    set_skill("shenkong-xing", 225);
    set_skill("dashou-yin", 225);	
    set_skill("yujiamu-quan", 225);	        	    	
	
	map_skill("force", "longxiang");
	map_skill("dodge", "shenkong-xing");
	map_skill("hand", "dashou-yin");
	map_skill("cuff", "yujiamu-quan");	
	map_skill("parry", "dashou-yin");	
	
	prepare_skill("hand", "dashou-yin");
	prepare_skill("cuff", "yujiamu-quan");

	set("chat_chance_combat", 80);
	set("chat_msg_combat", ({
		(: perform_action, "hand.jingang" :),
		(: perform_action, "cuff.chen" :),
		(: exert_function, "powerup" :),
		(: exert_function, "recover" :),
	}));		
	setup();
}

void die()
{
	object killer,ob;
	string id = random_ob[random(sizeof(random_ob))];//随机物品
	int exp;
	int num;
	if (objectp(killer = query_last_damage_from()))
	{
	    num=50+random(50);	
		killer->add("combat_exp",3600);
		killer->add("potential",2400);
	    killer->add("yuanbao_2",num);		
		tell_object(killer,"你杀死"+name()+"，获得3600点经验，2400点潜能"+num+"元宝票。\n");
	}
	switch (random(10))
	{
		case 0 : 
		case 5 :
		case 9 :
			
		   new(id)->move(killer);
		   new(id)->move(killer);
CHANNEL_D->do_channel(this_object(),"rumor", killer->query("name")+"在伏击金兵副本中获得了"+id->short()+"。");									
		break;
	}
	::die();
}
void unconcious()
{
	die();
}