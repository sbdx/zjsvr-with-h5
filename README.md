# zjsvr-with-h5

## 介绍
经典指尖服务和H5端；该版本是网络流传版， **可能存在漏洞和后门** ，仅供学习，欢迎参与优化。

## 软件架构
### h5需要的环境
> node 12.x 14.x ,node 相关依赖都已传到node_modules

### 项目编码
utf8


## 部署
> 该项目在win7 win10 win11 正常启动 linux未测试

### 配置文件
- 服务端 svr/config.hell
- H5测试时 使用google浏览器



### 启动
- 启动服务端 svr/startlib-64
- 启动h5端   h5/xaike.bat
- h5访问地址 http://127.0.0.1:3000


![输入图片说明](h5/image.png)
![输入图片说明](h5/image2.png)

## 参与优化

> h5端使用了jq mobeil 
- jm  官网 https://jquerymobile.com/
- Api 手册 https://www.codesocang.com/jquerymobile/jquerymobile/2/index.html


## 联系
Q群 391486684

